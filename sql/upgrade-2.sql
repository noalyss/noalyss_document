begin;

drop table if exists noalyss_document.acc_operation_detail ;

create table noalyss_document.acc_operation_detail (
    acc_operation_detail_id serial primary key,
    acc_operation_id int references noalyss_document.acc_operation(acc_operation_id) on update cascade on delete cascade not null,
    purchase_id int default null references public.fiche(f_id) on update cascade on delete set null,
    aod_amount numeric(20,4) default 0,
    tva_id int default null references public.tva_rate(tva_id) on update cascade on delete set null,
    aod_check int default 0 not null,
    aod_error text
);
insert into noalyss_document.acc_operation_detail(acc_operation_id,aod_amount,purchase_id,tva_id) 
    select acc_operation_id,ao_amount,purchase_id ,tva_id
    from noalyss_document.acc_operation;


comment on table noalyss_document.acc_operation_detail is 'the items for operation ';
comment on column noalyss_document.acc_operation_detail.tva_id  is 'TVA_RATE.TVA_ID';
comment on column noalyss_document.acc_operation_detail.purchase_id is 'Card id of item ';
comment on column noalyss_document.acc_operation_detail.aod_amount is 'Amount ';
comment on column noalyss_document.acc_operation_detail.aod_check is 'check : 0 not checked , 1 correct , 2 error';
comment on column noalyss_document.acc_operation_detail.aod_error is 'error description is aod_check is 2 ';


alter table noalyss_document.acc_operation drop column purchase_id;
alter table noalyss_document.acc_operation drop column tva_id;
alter table noalyss_document.acc_operation drop column ao_amount;

insert into noalyss_document.version values(3,'Allow several item per operation',now());
commit;