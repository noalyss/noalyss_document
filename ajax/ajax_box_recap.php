<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
// Copyright Author Dany De Bontridder danydb@aevalys.eu 24-07-23
/*!
 * \file
 * \brief Display a box with a similar document
 * \see Document_Operation::waiting_box
 * \see Document_Operation::display_warning_duplicate
 */
if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');

/**
 * @file
 * @brief display detail for a document
 */
try
{
    $p_document_id=$http->request("document_id","number");
}
catch (Exception $exc)
{
    echo $exc->getMessage();
    error_log($exc->getTraceAsString());
    return;
}

$document=new \Noalyss_Document\Document_Operation(new \Noalyss_Document\Document_SQL($cn,$p_document_id));

ob_start();
$document->display_recap();
$html=ob_get_contents();
ob_end_clean();
$a_ledger=$g_user->get_ledger('ACH', 3, false);
$status="OK";
if (empty($a_ledger))
{
    $status = 'NOLEDGER';$html="";
}
$answer=["status"=>$status, "message"=>$html];
\header('Content-Type: application/json;charset=utf-8');
echo json_encode($answer, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_NUMERIC_CHECK);

