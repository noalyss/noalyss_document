<?php
namespace Noalyss_Document;
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2022) Author Dany De Bontridder <danydb@noalyss.eu>


/**
 * @file
 * @brief ORM document
 * create table noalyss_document.acc_operation_detail (
    acc_operation_detail_id serial primary key,
    acc_operation_id int references noalyss_document.acc_operation(acc_operation_id) on update cascade on delete cascade,
    purchase_id int default null references public.fiche(f_id) on update cascade on delete set null,
    aod_amount numeric(20,4) default 0,
    tva_id int default null references public.tva_rate(tva_id) on update cascade on delete set null,
    j_id int references jrnx(j_id) on update cascade on delete set null
);

 */
class Acc_Operation_Detail_SQL extends \Table_Data_SQL
{

    function __construct(&$p_cn, $p_id=-1)
    {
        $this->table="noalyss_document.acc_operation_detail";
        $this->primary_key="acc_operation_detail_id";

        $this->name=array(
            "acc_operation_detail_id"=>"acc_operation_detail_id"
            , "acc_operation_id"=>"acc_operation_id"
            , "purchase_id"=>'purchase_id'
            , 'aod_amount'=>'aod_amount'
            , 'tva_id'=>'tva_id'
            , 'aod_check'=>'aod_check'
            , "aod_error"=>"aod_error"
            , "aod_amount_vat"=>"aod_amount_vat"
        );
        $this->type=array(
            "acc_operation_detail_id"=>"number"
            , "acc_operation_id"=>"number"
            , "purchase_id"=>'number'
            , 'aod_amount'=>'number'
            , 'tva_id'=>'number'
            ,'aod_check'=>"number"
            , "aod_error"=>"text"
            ,"aod_amount_vat"=>"number"
        );
        $this->default=array(
            "acc_operation_detail_id"=>"auto"
        );
        global $cn;

        parent::__construct($cn, $p_id);
    }

}