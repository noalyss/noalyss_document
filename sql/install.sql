-- install schema for Noalyss Document
-- GPL2 see license.txt
begin;
create schema noalyss_document ; 
comment on schema noalyss_document is 'Schema for pluggin NOALYSS-Document';


create table noalyss_document.version(version_id int primary key, 
    v_note varchar(100) not null, 
    v_date timestamp  not null);
insert into noalyss_document.version values(1,'install plugin',now());

commit;
