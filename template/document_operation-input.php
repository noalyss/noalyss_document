<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2021) Author Dany De Bontridder <danydb@noalyss.eu>

/**
 * @var $g_user  object global variable \Noalyss_User
 * @var $next_document_id int parameter passed to Document_Operation->input
 * @var $ident int use to create the string  DOMID of this document / operation , it is the document_id
 * @var $this \Noalyss_Document\Document_Operation
 */
if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');
\Noalyss\Dbg::echo_file(__FILE__);

global $g_user;
/**
 * @file
 * @brief form to input acc operation
 */
$a_ledger=$g_user->get_ledger('ACH', 3, false);
if (empty($a_ledger))
{
    echo_warning(_("Aucun journal disponible"));
    return;
}
$ident=$this->document_sql->document_id;
$document_operation=\Noalyss_Document\Document_Operation::from_document_id($ident);
$acc_operation_sql=$document_operation->getAcc_operation_sql();
$cn=$acc_operation_sql->cn;

$readonly=($acc_operation_sql->getp("ao_status")==1)?true:false;
$date_op=new IDate("date_op");
$date_op->readOnly=$readonly;
$date_op->id=$date_op->name.$ident;
$date_op->value=substr($acc_operation_sql->getp("ao_date")??"", 0, 10);
$card=new ICard("supplier".$ident);
$supplier_id=$acc_operation_sql->getp("supplier_id");
$card->value=($supplier_id!="")?$cn->get_value("select ad_value from fiche_detail 
         where ad_id=23 and f_id=$1", [$supplier_id]):"";

$str_ledger=""; $and="";
foreach ($a_ledger as $ledger)
{
    $str_ledger.=$and.$ledger['jrn_def_id'];
    $and=",";
}
$cn=Dossier::connect();
$a_card=$cn->get_array("select jrn_def_fiche_deb,jrn_def_fiche_cred from jrn_def where jrn_def_id in($str_ledger)");
$str_deb=""; $and_deb=""; $str_cred=""; $and_cred="";
foreach ($a_card as $item_card)
{
    $str_deb.=$and_deb.$item_card['jrn_def_fiche_deb'];
    $str_cred.=$and_cred.$item_card['jrn_def_fiche_cred'];
    $and_deb=",";
    $and_cred=",";
}
$card->set_typecard($str_cred);
$card->set_limit(5);
$card->readOnly=$readonly;
$card->style=sprintf(' placeholder="%s"', _("Fournisseur"));
$card->set_choice("found_cards_div".$ident);
$card->set_choice_create(false);
$card->set_dblclick("fill_ipopcard(this);");
$card->set_fct("null");
$note=new \Itext("note");
$note->value=$acc_operation_sql->getp("ao_label");
$note->size="40";
$note->placeholder=_('libellé opération');
$note->readOnly=$readonly;
// Note of the operation
$ao_note=new \ITextarea("ao_note");
$ao_note->value=$acc_operation_sql->getp("ao_note");
$ao_note->placeholder=_("Note interne");
$ao_note->readOnly=$readonly;

// Extra info for payment
$ao_info=new \IText("ao_info");
$ao_info->value=$acc_operation_sql->getp("ao_info");
$ao_info->placeholder=_("info bancaire ou autre");
$ao_info->readOnly=$readonly;

if (        $acc_operation_sql->ao_status != 1) {
    $ready=new ICheckBox("ready",3);
    $ready->set_check($acc_operation_sql->ao_status );
    $str_ready=" Prêt à transférer : {$ready->input()}" ;
} else {
    $str_ready="";
}

?>
<?php if (!$readonly): ?>
    <form id="acc_<?= $ident ?>" onsubmit="noalyss_document.acc_operation_save(this);return false;">
    <?php else: ?>
        <?php
        $jr_id=$cn->get_value('select jr_id from jrn where jr_internal =$1', [$acc_operation_sql->getp('jr_internal')]);
        ?>
        <p>
            <?= _('Transféré') ?> <?= $acc_operation_sql->getp("ao_saved") ?> 
            <?= \HtmlInput::detail_op($jr_id, $acc_operation_sql->getp('jr_internal')) ?>
        </p>
    <?php endif; ?>    
    <p>

        <?= HtmlInput::hidden('document_id', $ident) ?>
        <?= HtmlInput::hidden('act', "acc_operation_save") ?>
        <?= HtmlInput::get_to_hidden(array("gDossier", "ac", "plugin_code")) ?>

        <?= $date_op->input() ?>
        <?= $note->input() ?>
        <button class="smallbutton" onclick="$('extra_info_div<?=$ident?>').show();return false;">
            <?=_("Autre info")?>
            <?=\Icon_Action::show_icon(uniqid(),"$('extra_info_div{$ident}').show();return false;")?>
        </button>
    </p>
        <div id="extra_info_div<?=$ident?>" style="display: none" class="inner_box">
            <div>
                <h2 class="title"><?=_("Information supplémentaires")?></h2>
                <div>
                    <?=_("Note")?>
                </div>
            <?=$ao_note->input()?>

            </div>
            <div style="margin-top:0.5rem">
            <?=_("Communication")?><?=$ao_info->input()?>

            </div>
            <?=\HtmlInput::button_hide("extra_info_div{$ident}");?>
        </div>
        <div>
            <?php if (!$readonly): ?>
            <?php echo _("Ajout d'étiquettes");?>
            <?php echo Tag_Action::select_tag_search('add'.$ident); ?>
            <?php echo Tag_Action::add_clear_button('add'.$ident); ?>
            <?php endif;?>
            <span id="add<?=$ident?>tag_choose_td">
                <?php
                /*show existing tags */
                $ao_tag=$acc_operation_sql->getp("ao_tag");
                if ( ! empty ($ao_tag)) {
                    $aTag_id=explode(",",$ao_tag);
                    foreach ($aTag_id as $tag_id) {
                        $tag=new \Tag_Action($cn,$tag_id);
                        $tag->update_search_cell("add".$ident);
                    }
                }
                ?>

            </span>
        </div>
    <p>
        <?= $card->input() ?>
        <?= $card->search() ?>
    </p>
    <div id="detail<?= $ident ?>_div">
        <?php
        $cnt=$this->display_detail($str_deb, $readonly,$ident);
        echo \HtmlInput::hidden("nb_item".$ident, $cnt);
        ?>
    </div>
    <?php if (!$readonly): ?>
        <div>
           <?=$str_ready?>

        </div>
        <?= \Icon_Action::Icon_Add(uniqid(), "noalyss_document.row_add('$ident', '$str_deb')") ?>

        <?= HtmlInput::submit("save", "Sauve") ?>
        <?php
        //-------------------------------------------------------------------------------------------
        // propose to copy info to XMLPY , for payment
        // save first and then copy
        //
        //-------------------------------------------------------------------------------------------

        // check user can use XMLPY ,
        if ($g_user->check_module('XMLPY') == 1 && $cn->exist_schema('xmlpayment')) {
            $this->propose_copy_xmlpy();
        }


        ?>
        <?php
        // if a document follows , propose to close and save the current one and open the next document
        if ( $next_document_id != 0 ) {
            $js=sprintf("noalyss_document.more(this,'%s');",$ident);
            $js.=sprintf("noalyss_document.acc_operation_save($('acc_%s'));",$ident);
            $js.=sprintf("noalyss_document.more(this,'%s')",$next_document_id);
            //$js.=sprintf(";$('row%s').scrollTo();",$next_document_id);
            // $js.=";window.scrollByLines(10)";
            echo HtmlInput::button_action(_("Suivant"),$js,                uniqid(),"smallbutton");
        }
        ?>


        <?= HtmlInput::button_action(_("Fermer"),"noalyss_document.more(this,'$ident');return false;",
                uniqid(),"smallbutton") ?>
        <div style="position:static;margin-top:0.5rem;height:6.5rem" >
            <div style="position:static; " class="autocomplete" id="found_cards_div<?= $ident ?>">
            </div>
        </div>
    </form>
    <?php else: ?>
        <?= HtmlInput::button_action(_("Fermer"),"noalyss_document.more(this,'$ident');return false;",
                uniqid(),"smallbutton") ?>
<?php endif; ?>

<div class="sum_operation" >
    <div>
        <?php
        echo _('Total');
        ?>
    </div>
    <div id="sum_operation<?=$ident?>">
        <?=$this->display_total()?>
    </div>
</div>
<p id="feedback<?= $ident ?>" ></p>
<p class="notice"><?= $acc_operation_sql->getp("ao_message") ?></p>
<p><?= _("Dernier changement") ?> <?= $acc_operation_sql->getp("ao_saved") ?></p>


