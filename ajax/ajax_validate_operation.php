<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2021) Author Dany De Bontridder <danydb@noalyss.eu>

if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');
global $cn;
/**
 * @file
 * @brief Validate the selected operations
 */
try
{
    $a_document=$http->post("a_document");
    $ledger_id=$http->post("p_jrn", "number");
    if (empty($a_document))
    {
        throw new Exception(_("AVO38.Vide"));
    }
}
catch (Exception $exc)
{
    echo $exc->getMessage();
    error_log($exc->getTraceAsString());
    exit();
}

// retrieve all the operation 
//
//  for each check :
//  - TVA exists, 
//  - card belong to that ledger
//  - amount exists
$a_valide=array();
$first_date="";
$a_document_obj = array();

foreach ($a_document as $document_id)
{
    $document_operation=\Noalyss_Document\Document_Operation::from_document_id($document_id);
    $document_operation->clean_empty();
    $date="";
    if ($document_operation->check_transferable($ledger_id)==true)
    {
        $a_valide[]=$document_operation;
        $date=substr($document_operation->getAcc_operation_sql()->ao_date, 0, 10);
        $a_date=explode('.', $date);
        $date=$a_date[2].".".$a_date[1].".".$a_date[0];
        if ($first_date=="")
        {
            $first_date=$date;
        }
        elseif ($date< $first_date)
        {
            $first_date=$date;
        }
    }
    $a_document_obj[]=["date"=>$date,"doc"=>$document_operation];
}
// -------------------------------
// Sort operation by date
// -------------------------------
asort($a_document_obj);

// -------------------------------
// Display Operation
// -------------------------------
$nb_error=0;
foreach ($a_document_obj as $document_obj) {
    $nb_error += $document_obj['doc']->display_result();
}
if ($nb_error != 0 ) {
    echo '<span class="warning">';
    printf (_("Attention il y a %s erreurs , vous devriez les corriger avant de transférer"),$nb_error);
    echo  '</span>';

}
if (empty($a_valide))
{
    echo '<span class="warning">', _("Aucune opération à transférer"), "</span>";
    echo '<button class="smallbutton" onclick="$(\'document_validate_div\').hide(),$(\'document_list_div\').show();">', _("Retour"),
    '</button>';

    return;
}

$last_date_ledger=$cn->get_value("select to_char(max(jr_date),'YYYY.MM.DD') from jrn where jr_def_id=$1", [$ledger_id]);

echo '<p class="notice">', _("Opération "), $first_date;
echo _("Dernière opération du journal "), $last_date_ledger, "</p>";


?>
<form method="POST">
    
    <?= \HtmlInput::post_to_hidden(["gDossier", "ac", "plugin_code"]) ?>
    <?php
    foreach ($a_valide as $i_valide)
    {
        echo \HtmlInput::hidden("document_id[]", $i_valide->get_document_sql()->getp("document_id"));
    }
    $single_record=new \Single_Record("transfer_accountancy");
    echo $single_record->hidden();
    echo \HtmlInput::hidden("sb", "transfer");
    echo \HtmlInput::hidden("ledger_id", $ledger_id);
    if ($first_date < $last_date_ledger)
    {
        $checkbox=new \ICheckBox("correct_date",1);
        echo '<span class="notice">',
        _('Attention,vous allez ajouter des opérations qui sont avant la dernière opération de ce journal'),
        "</span>";
        echo '<p>', _("Le numéro des pièces ne suivra pas l'ordre chronologique.");
        echo _("La date de facture sera sauvegardée dans le libellé d'opération");
        echo '</p>';
        echo '<p>', _("Opération "), $first_date,"  => ";
        echo  _("Dernière opération du journal "), $last_date_ledger, "</p>";
        echo '<p>';
        echo p(sprintf(_('voulez-vous que les opérations avant %s soient mises au %s ?'),
                $last_date_ledger,$last_date_ledger));
        $checkbox->selected=1;
        echo span($checkbox->input()." "._("Cochez pour remplacer la date de facture par la date de comptabilisation, la date de comptabilitsation donne la période fiscale,
        il est impératif de la comptabilité soit tenue dans l'ordre chronologique et suive l'ordre de la comptabilisation"),'class="notice" style="display:block;width:50%;margin-left:10%"');
    }
    echo \HtmlInput::submit("submit_transfer", _("Transférer"));
    ?>
    <button class="smallbutton" 
            onclick="$('document_validate_div').hide(); $('document_list_div').show();return false;">
        <?= _("Retour") ?>
    </button>
</form>

