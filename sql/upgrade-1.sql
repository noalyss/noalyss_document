begin;
drop table if exists noalyss_document.document ;

create table noalyss_document.document (
    document_id serial primary key, 
    d_name varchar(200) not null, 
    d_type varchar(200) not null ,
    d_size bigint not null,
    d_uploaded_time timestamp default now() not null,
    d_lob oid );

comment on table noalyss_document.document  is 'Record all the uploaded documents';
comment on column noalyss_document.document.d_name is 'File name';
comment on column noalyss_document.document.d_type  is 'Type of the file ';
comment on column noalyss_document.document.d_size  is 'Size of the file in Bytes ';
comment on column noalyss_document.document.d_lob is 'oid of the file ';
comment on column noalyss_document.document.d_uploaded_time is 'time of the uploading ';

drop table if exists noalyss_document.acc_operation ;
create table noalyss_document.acc_operation (
    acc_operation_id serial primary key,
    ao_label  varchar (200),
    ao_date date ,
    supplier_id int ,
    purchase_id int ,
    ao_amount numeric(20,4),
    document_id bigint not null references noalyss_document.document(document_id) on delete cascade on update cascade,
    ao_saved timestamp default now(),
    ao_status int default 0,
    ao_message varchar(200),
    tva_id  int,
    jr_internal   text
);


comment on table noalyss_document.acc_operation  is 'Record all the operation for documents';
comment on column noalyss_document.acc_operation.ao_date is 'Date of operation';
comment on column noalyss_document.acc_operation.ao_label is 'Label of operation';
comment on column noalyss_document.acc_operation.supplier_id is 'Card id of supplier';
comment on column noalyss_document.acc_operation.purchase_id is 'Card id of item ';
comment on column noalyss_document.acc_operation.ao_amount is 'Amount ';
comment on column noalyss_document.acc_operation.document_id is 'FK to document(document_id)';
comment on column noalyss_document.acc_operation.ao_status is '0 not transferred , 1 transferred , 2 error, 3 ready';
comment on column noalyss_document.acc_operation.ao_message is 'if oa_status == 2 detail of error';
comment on column noalyss_document.acc_operation.tva_id  is 'TVA_RATE.TVA_ID';
comment on column noalyss_document.acc_operation.jr_internal is 'JRN.JR_INTERNAL';

CREATE OR REPLACE FUNCTION  noalyss_document.acc_operation_set_oa_saved()
 RETURNS trigger
AS $function$
begin
	new.ao_saved=now();
	return new;
end ;
$function$
 LANGUAGE plpgsql;

create trigger trg_acc_operation_oa_saved before insert or update on noalyss_document.acc_operation  for each  row execute procedure noalyss_document.acc_operation_set_oa_saved();

create table noalyss_document.followup
(
    fo_id serial primary key,
    ag_id int not null ,
    document_id int references noalyss_document."document"(document_id) on delete cascade on update cascade
);

insert into noalyss_document.version values(2,'install tables',now());
commit;
