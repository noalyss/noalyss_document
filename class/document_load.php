<?php

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2021) Author Dany De Bontridder <danydb@noalyss.eu>

/**
 * @file
 * @brief  Document_Load manage the documents for loading and save it to db
 */

namespace Noalyss_Document;

class Document_Load
{

    private $cn; //!< Database conx

    /**
     * * @brief  Document_Load manage the documents for loading and save it to db
     * 
     */
    function __construct(\Database $cn)
    {
        $this->cn=$cn;
    }

    /**
     * @brief save the document received from posting the form into db, returns an array with the result
     * , index of this array is 
     *    - 'nb_error'     => nb of errors, 
     *    - 'nb_success'   => nb of successfull upload, 
     *    - 'msg_error'    => error message and 
     *    - 'msg_success'  => successfull file name
     * 
     * @param string $p_file Name of $_FILES, 
     */
    static function save_file($p_file)
    {
        $cn=\Dossier::connect();
        if (isset($_FILES[$p_file]))
        {

            $error_msg=array();
            $error=0; $success=0; $saved=array();
            $file=$_FILES[$p_file];

            $new_name=tempnam($_ENV['TMP'], 'document_noalyss_');
            if (is_array($file['tmp_name']))
            {
                $nb_file=\count($file['tmp_name']);
            }
            for ($i=0; $i<$nb_file; $i++)
            {
                $file_tmp_name=$file['tmp_name'][$i];
                $file_error=$file['error'][$i];
                $file_name=$file['name'][$i];
                $file_type=$file['type'][$i];
                $file_size=$file['size'][$i];
                if (in_array($file_type, ['image/jpeg', 'image/png']))
                {
                    $return=Document_Load::compress_file($file_tmp_name);
                    $file_size=$return['size'];
                    if ($return['type'] != $file_type) {
                        $file_name=str_replace($file_name,'.png','.jpg');
                    }
                    $file_type=$return['type'];
                }
                if (
                        $file_tmp_name!=''&&
                        $file_error==0&&
                        move_uploaded_file($file_tmp_name, $new_name))
                {
                    $cn->start();
                    $oid=$cn->lo_import($new_name);
                    if ($oid==false)
                    {
                        $error_message.=sprintf(_("Echec import base de données %s"), $file['name'][$i]);
                        $error++;
                    }
                    else
                    {
                        $document=new Document_SQL($cn);
                        $document->setp("d_name", $file_name);
                        $document->setp("d_size", $file_size);
                        $document->setp("d_type", $file_type);
                        $document->setp("d_lob", $oid);
                        $document->insert();
                        $saved[]=$file_name;
                        $success++;
                    }
                    $cn->commit();
                }
                else
                {
                    $error_msg[]=$file_name;
                    $error++;
                }
            }
            return (["nb_error"=>$error, 'nb_success'=>$success, 'msg_error'=>$error_msg, 'msg_success'=>$saved]);
        }
        return (["nb_error"=>1, 'nb_success'=>0, 'msg_error'=>[ _("Aucun fichier accepté")], 'msg_success'=>""]);
    }

    /**
     * @brief display all the documents loaded in the db
     * @global type $g_dir_noadoc
     * @param int $p_filter , filter the document :-1 all, 0 not transferred only, 1 only transferred,2 only errors
     */
    function display_table($p_filter=-1)
    {
        global $g_dir_noadoc;

        global $gDossier, $g_access, $g_plugin;


        if ($p_filter==-1)
        {
            $filter="";
        }
        elseif (isNumber($p_filter)==1)
        {
            $filter='where  coalesce(ao_status ,0) = '.$p_filter;
        }
        else
        {
            throw new Exception(_("DL121 invalid filter"), 121);
        }
        $http=new \HttpInput();
        $sort_table = new \Sort_Table();

        $url = "?" . http_build_query(array("gDossier" => $gDossier,
                "ac" => $g_access,
                "plugin_code" => $g_plugin,
                "sa"=>$http->request("sa"),
                "filter_document_value"=>$http->request("filter_document_value","string","0")));

        $sort_table->add(_("Fichier"), $url, " order by ao_date asc,d1.d_name asc",
            "order by ao_date desc,d1.d_name desc","fa","fd");

        $sort_table->add(_("Date"), $url, " order by d1.d_uploaded_time asc",
            "order by d1.d_uploaded_time desc","da","dd");

        $sort_table->add(_("Montant"), $url, " order by  coalesce(sd.sum_op,0) asc",
            "order by coalesce(sd.sum_op,0) desc","ma","md");

         $sql_order = $sort_table->get_sql_order($http->request("ord","string","da"));

        $ret=$this->cn->exec_sql("
with sum_detail as (select sum(aod_amount) sum_op,acc_operation_id  
from noalyss_document.acc_operation_detail aod group by acc_operation_id  )  ,
tag_1 as (
select acc_operation_id,string_agg( t_tag::text,'~~') list_tag,string_agg( t_color::text,'-') list_tag_color
from (select acc_operation_id , regexp_split_to_table(ao_tag,',') tg from noalyss_document.acc_operation ) ao  
join tags t on (t.t_id=ao.tg::numeric) 
group by acc_operation_id
)
   SELECT d1.document_id, 
                    d_name, 
                    d_type, 
                    d_size, 
                    d_uploaded_time, 
                    d_lob ,
                    ao.acc_operation_id, 
                    ao_label, 
                    ao_date, 
                    supplier_id, 
                    (select ad_value from fiche_detail where f_id=supplier_id and ad_id=23) qcode,
                    document_id, 
                    ao_saved, 
                    ao_status, 
                    ao_message, 
                    jr_internal ,
                    (select count(*) from noalyss_document.followup f where f.document_id=d1.document_id) as cnt_follow,
                    sd.sum_op,
                    list_tag,
                    list_tag_color
           FROM noalyss_document.\"document\"  as d1
           left join noalyss_document.acc_operation ao using (document_id)
           left join sum_detail sd on (ao.acc_operation_id=sd.acc_operation_id)
           left join tag_1 on (ao.acc_operation_id=tag_1.acc_operation_id)
             $filter
               {$sql_order}
               ");

        $max_row=\Database::num_row($ret);

        require_once $g_dir_noadoc."/template/document_load-display_table.php";
    }

    /**
     * @brief display documents 
     * @param string pa_document_id array of document_id string of id separated by a comma
     */
    static function delete_document($pa_doc_id)
    {
        global $cn;

        $a_doc_id=explode(",", $pa_doc_id);
        if (empty($pa_doc_id))
        {
            return;
        }
        try
        {
            $cn->start();
            foreach ($a_doc_id as $iDocId)
            {
                $document_sql=new Document_SQL($cn, $iDocId);
                $row=$cn->get_value('select count(d.document_id)
                        from noalyss_document."document"  d
                        left join noalyss_document.acc_operation using (document_id) 
                        left join noalyss_document.followup using (document_id) 
                        where d.document_id=$1 and (coalesce(fo_id,0) <> 0  or coalesce(ao_status,0) <> 0) ', [$iDocId]);
                if ($row == 0 )
                {

                    \Noalyss\Dbg::echo_var(1,"delete lob $row");
                    $cn->lo_unlink($document_sql->getp("d_lob"));

                }
                \Noalyss\Dbg::echo_var(1,"Row to delete $document_sql");
                $document_sql->delete();
            }
            $cn->commit();
        }
        catch (Exception $exc)
        {
            $cn->rollback();
            echo $exc->getMessage();
            error_log($exc->getTraceAsString());
        }
    }

    /**
     * @brief add an empty document
     */
    static public function add_empty_document()
    {
        global $cn;

        $document_sql=new Document_SQL($cn);
        $document_sql->setp("d_name", "Document vide");
        $document_sql->setp("d_size", "0");
        $document_sql->setp("d_type", "application/txt");
        $document_sql->save();
    }

    /**
     * @brief compress the images files 
     * @returns array [ size=>in byte , 'type'=>'mime type' ]
     */
    static function compress_file($file_tmp_name)
    {

        $info=getimagesize($file_tmp_name);
        if ($info[0]<=WIDTH)
        {
            return array('size'=> filesize($file_tmp_name),'type'=>$info['mime']);
        }
        if ($info['mime']=='image/jpeg')
        {
            $jpg_image=imagecreatefromjpeg($file_tmp_name);
        }
        elseif ($info['mime']=='image/png')
        {
            $jpg_image=imagecreatefrompng($file_tmp_name);
        }
        else
        {
            return array('size'=> filesize($file_tmp_name),'type'=>$info['mime']);
        }

        $jpg_imagescale=imagescale($jpg_image, WIDTH);
        $jpg_image=$jpg_imagescale;
        imagejpeg($jpg_image, $file_tmp_name, 90);
        return array('size'=>filesize($file_tmp_name),'type'=>'image/jpeg');
    }
    /**
     * @brief convert a value in Mbytes, kb ... in byte
     * @param type $p_value
     * @return type
     */
    function transform_byte($p_value)
    {
        $a_convert=["k"=>1024,"m"=>1024**2,"g"=>1024**3];
        
        $p_value=trim($p_value);
        $last=strtolower($p_value[strlen($p_value)-1]);
        $p_value=substr($p_value,0,strlen($p_value)-1);
        if ( isset($a_convert[$last])) {
            $p_value=$p_value*$a_convert[$last];
        }

        return $p_value;
    }
    /**
     * @brief Get the max size for a form POST 
     * @return type
     */
    function get_max_post()
    {
        $max=ini_get("post_max_size");
        return $this->transform_byte($max);
    }

    /**
     * @brief display the summary of a document + operations saved + tag if any, called from display_table
     * @see Document_Load::display_table()
     * @param int $document_id SQL NOALYSS_DOCUMENT.DOCUMENT.DOCUMENT_ID
     * @param int $document_status SQL NOALYSS_DOCUMENT.ACC_OPERATION.AO_STATUS
     * @param date  $operation_date ((YYYY.MM.DD ) SQL NOALYSS_DOCUMENT.ACC_OPERATION.AO_DATE
     * @param string $filename name of the file SQL NOALYSS_DOCUMENT.D_NAME
     * @param string $qcode qcode of the supplier from SQL NOALYSS_DOCUMENT.ACC_OPERATION.SUPPLIER_ID
     * @param string $label label of operation SQL NOALYSS_DOCUMENT.ACC_OPERATION.AO_LABEL
     * @param number $sum_operation sum of the operation (from NOALYSS_DOCUMENT.ACC_OPERATION_DETAIL.AOD_AMOUNT)
     * @param int $follow_up number of event in followup with document (from NOALYSS_DOCUMENT.FOLLOWUP )
     * @param string $list_tag string with tag label separated by ~~ from SQL NOALYSS_DOCUMENT.ACC_OPERATION.AO_TAGS)
     * @param string  $list_tag_color list of integer separated by comma from NOALYSS_DOCUMENT.ACC_OPERATION.AO_TAGS & PUBLIC.TAG
     * @return void
     */
    static function display_recap(  $document_id
                                    ,$document_status
                                    ,$operation_date
                                    ,$filename
                                    ,$qcode
                                    ,$label
                                    ,$sum_operation
                                    ,$follow_up
                                    ,$list_tag
                                    ,$list_tag_color)
    {
        global $g_dir_noadoc;
        include $g_dir_noadoc."/template/document_load-display_recap.php";
    }

    /**
     * @brief Display the status of a document : New, transferred, error ...
     * @param $document_status
     * @return void
     */
    static function display_status($document_status)
    {
        if ($document_status==1):
            echo span(_("Transferé"),
                ' style="background-color:darkgreen;color:white;;padding:0px 1.5rem 0px 1.5rem;margin:2px;border-radius:5px"');
        elseif ($document_status==0):
            echo span(_("Nouveau"),
                ' style="background-color:pink;color:white;;padding:0px 1.5rem 0px 1.5rem;margin:2px;border-radius:5px"');
        elseif ($document_status==2):
            echo span(_("Error"),
                ' style="background-color:red;color:white;;padding:0px 1.5rem 0px 1.5rem;margin:2px;border-radius:5px"');
        elseif ($document_status==3):
            echo span(_("Prêt"),
                ' style="background-color:lightgreen;color:darkgreen;padding:0px 1.5rem 0px 1.5rem;margin:2px;border-radius:5px"');
        else:
            printf(_("Inconnu"));
        endif;
    }

}
