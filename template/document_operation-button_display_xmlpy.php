<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
// Copyright Author Dany De Bontridder danydb@aevalys.eu 16/11/24
/*! 
 * \file
 * \brief display existing record in XMLPY
 */
/**
 * @var $this \Noalyss_Document\Document_Operation
 * @var $g_user \Noalyss_User
 * @var $detail array from Document_Operation->button_display_xmlpy
 * @var $cn \Database from  Document_Operation->button_display_xmlpy
 */

if (!defined('ALLOWED')) {
    die ("not allowed");
}
$f_id=( $detail['fiche_id']=='') ? $f_id=0 : $detail['fiche_id'];

$fiche=new Fiche($cn,$f_id);
$url="?".http_build_query(['gDossier'=>\Dossier::id(),'sa'=>'list_detail','p_id'=>$detail['payment_id'],'plugin_code'=>'XMLPY','ac'=>'XMLPY']);


?>
<div>

    <h3>
    <a href="<?=$url?>" target="_blank" class="line">
        Liste : <?=$detail['payment_id']?>

    </a>
    </h3>
    <div>
        Nom : <?=$fiche->strAttribut(ATTR_DEF_NAME,0)?>
    </div>
    <div>
        qcode : <?=$fiche->strAttribut(ATTR_DEF_QUICKCODE,0)?>
    </div>
    <div>
        Banque : <?=$fiche->strAttribut(ATTR_DEF_BQ_NO,0)?>
    </div>
    <div>
        Montant : <?=nbm($detail['d_amount'],2)?>
    </div>
    <div>
        Communication <?=$detail['d_message']?>
    </div>
</div>

