<?php

namespace Noalyss_Document;

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2022) Author Dany De Bontridder <danydb@noalyss.eu>

/**
 * @file
 * @brief handle the accountancy operation  for document
 */
class Document_Operation
{

    private  Document_SQL $document_sql; //!< Document_SQL
    private Acc_Operation_SQL $acc_operation_sql; //!< Acc_Operation_SQL

    function __construct(Document_SQL $p_document_sql)
    {
        $this->document_sql=$p_document_sql;
        $this->find_operation();

    }

    static function array_status() {
        return [0=>_("Non transféré"),1=>_("Transféré"),2=>_("Erreur"), 3=>_("Prêt")];
    }
    static function from_document_id($p_document_id)
    {
        $cn=\Dossier::connect();
        $document_sql=new Document_SQL($cn, $p_document_id);
        $document_operation=new Document_Operation($document_sql);
        return $document_operation;
    }

    /**
     * @brief find acc_operation
     */
    public function find_operation()
    {
        $cn=$this->document_sql->cn;

        $acc_operation_id=$cn->get_value("select acc_operation_id 
                 from noalyss_document.acc_operation 
                 where document_id=$1", [$this->document_sql->document_id]);

        if ($cn->get_affected()==0)
        {
            $acc_operation_id=-1;
        }

        $this->acc_operation_sql=new Acc_Operation_SQL($cn, $acc_operation_id);
        return $this->acc_operation_sql;
    }

    public function getAcc_operation_sql()
    {
        return $this->acc_operation_sql;
    }

    public function setAcc_operation_sql($acc_operation_sql)
    {
        $this->acc_operation_sql=$acc_operation_sql;
        return $this;
    }

    public function get_document_sql()
    {
        return $this->document_sql;
    }

    public function set_document_sql($p_document_sql)
    {
        $this->document_sql=$p_document_sql;
        $this->acc_operation_sql=$this->find_operation();
        return $this;
    }

    /***
     * @brief display the operation detail for a Document_SQL 
     */

    public function input($next_document_id)
    {
        global $g_dir_noadoc;
        require_once $g_dir_noadoc."/template/document_operation-input.php";
    }

    /**
     * @brief check the periode : Periode->find_periode throw an exception which is not catch
     */
    public function check_periode($p_date, $p_ledger_id)
    {
        $cn=$this->acc_operation_sql->cn;
        $periode=new \Periode($cn);
        $sql="select p_id from parm_periode where p_start <= to_date($1,'DD.MM.YYYY') and p_end >= to_date($1,'DD.MM.YYYY') ";
        $period_id=$cn->get_value($sql, array($p_date));
        $periode->jrn_def_id=$p_ledger_id;
        $periode->set_periode($period_id);
        if (isNumber($period_id)==0||$periode->is_closed()==1)
        {
            return false;
        }
        return true;
    }

    /**
     * @brief check and set the ACC_OPERATION.AO_MESSAGE and ACC_OPERATION.AO_STATUS
     *   - TVA exists,
     *   - accounting exists of CARD and VAT code
     *   - card belong to that ledger
     *   - amount valide
     * @param int $p_ledger_id JRN_DEF.JRN_DEF_ID
     */
    public function check_transferable($p_ledger_id)
    {
        $cn=$this->acc_operation_sql->cn;
        // if there is no acc_operation
        $this->acc_operation_sql->setp("document_id", $this->document_sql->getp("document_id"));
        $this->acc_operation_sql->load();
        if ( $this->acc_operation_sql->getp("ao_status")==1) {
            return false;
        }
        // check the date , 
        $ck_date=true;
        $date=substr($this->acc_operation_sql->getp("ao_date")??"", 0, 10);
        if (isDate($date)==null)
        {
            $ck_date=false;
        }

        $ck_periode=true;
        if (!$ck_date||$this->check_periode($date, $p_ledger_id)==false)
        {
            $ck_periode=false;
        }
        $a_row=$cn->get_row('select jrn_def_fiche_deb,jrn_def_fiche_cred from jrn_def where jrn_def_id=$1',
                [$p_ledger_id]);
        $cat_deb=$a_row['jrn_def_fiche_deb']; $cat_cred=$a_row["jrn_def_fiche_cred"];
        
        $cat_deb=(empty($cat_deb))?"-1":$cat_deb;
        $cat_cred=(empty($cat_cred))?"-1":$cat_cred;
        
        $ck_supplier=false;
        $supplier_id=$this->acc_operation_sql->getp('supplier_id');
        if ($cn->get_value("select count(*) from fiche where fd_id in (".\sql_string(trim($cat_cred)).") and f_id=$1",
                        array($supplier_id))==1)
        {
            $ck_supplier=true;
        }


        // from db , retrieve data for acc_operation_detail
        $a_operation_detail=$cn->get_array("select  * from noalyss_document.acc_operation_detail 
                where 
                 acc_operation_id=$1", [$this->acc_operation_sql->getp("acc_operation_id")]);
        $array['nb_item']=count($a_operation_detail);
        $error_detail=0;
        for ($e=0; $e<$array['nb_item']; $e++)
        {
            $error_message_detail="";
            // check amount
            $amount=$a_operation_detail[$e]["aod_amount"];
            $check_amount=false;
            if  ( empty ($amount) || isNumber($amount)==0||$amount==0)
            {
                continue;
            }
            // Check TVA
            $tva_id=$a_operation_detail[$e]["tva_id"];
            $check_tva=false;
            // check that VAT exists and also the accounting
            if (    !empty ($tva_id)  
                    && isNUmber($tva_id)==1
                    && $cn->get_value("select count(*) from tva_rate where tva_id=$1 and tva_poste not like '#,%'",
                            [$tva_id])==1
                    && $cn->get_value("select count(*) 
                        from tva_rate
                        where 
                            left(tva_poste ,position(',' in tva_poste)-1  ) not in (select pcm_val from tmp_pcmn)
                        and tva_id=$1",[$tva_id]) == 0)
            {
                $check_tva=true;
            }

            $ck_purchase=false;
            $purchase_id=$a_operation_detail[$e]['purchase_id'];
            // check that card exists and also its accounting
            if ($cn->get_value("select count(*) from fiche where fd_id in (".\sql_string(trim($cat_deb)).") and f_id=$1",
                            array($purchase_id))==1
                && $cn->get_value( "select count(*) 
                            from fiche_detail 
                            where 
                                f_id=$1 
                              and ad_id=5
                              and exists (select pcm_val from tmp_pcmn tp where pcm_val=ad_value)",[$purchase_id])!=0
                )
            {
                $ck_purchase=true;
            }
            $error_message_detail.=(!$check_tva)?_("TVA invalide ou poste comptable TVA inexistant").'-':"";
            $error_message_detail.=(!$ck_purchase)?_("Fiche service invalide").'-':"";
            $acc_operation_detail=new Acc_Operation_Detail_SQL($cn, $a_operation_detail[$e]['acc_operation_detail_id']);
            if (strlen($error_message_detail) > 0)
            {
                $acc_operation_detail->setp("aod_check", 2);
                $acc_operation_detail->setp("aod_error", $error_message_detail);
                $error_detail++;
            }
            else
            {
                $acc_operation_detail->setp("aod_check", 1);
                $acc_operation_detail->setp("aod_error", null);
            }
            $acc_operation_detail->save();
        }
        $error_message="";
        if ( $array['nb_item'] == 0 ) {
            $error_message.=_("Aucun bien ou service");
        }

        $error_message.=(!$ck_supplier)?_("Fiche fournisseur  invalide").'-':"";
        $error_message.=(!$ck_periode)?_("Erreur période").'-':"";
        $error_message.=(!$ck_date)?_("Erreur date").'-':"";
        $error_message.=($error_detail>0)?_("Erreur élément").'-'.$error_message_detail:"";

        if (strlen($error_message)>0||$error_detail>0)
        {
            $this->acc_operation_sql->setp("ao_status", 2);
            $this->acc_operation_sql->setp("ao_message", $error_message);
            $this->acc_operation_sql->save();
            return false;
        }
        $this->acc_operation_sql->setp("ao_status", 3);
        $this->acc_operation_sql->setp("ao_message", "");
        $this->acc_operation_sql->save();

        try
        {
            $array=$this->transform($p_ledger_id);
            $ledger=new \Acc_Ledger_Purchase($cn, $p_ledger_id);
            $ledger->verify($array);
        }
        catch (\Exception $ex)
        {
            $this->acc_operation_sql->setp("ao_status", 2);
            $this->acc_operation_sql->setp("ao_message", $ex->getMessage());
            $this->acc_operation_sql->save();
            return false;
        }
        return true;
    }
    private function display_col_header() {
        echo <<<EOF
<div class="row highlight" style="background-color: #9CAFD4;padding:1rem;color:wheat;">
    <div class="col" style="border :">
        Date
    </div>
    <div class="col-3">
        Document
    </div>
    <div class="col">
     Fournisseur
    </div>
    <div class="col">
        Description
    </div>
    <div class="col" style="text-align: right">
    Montant
    </div>
     <div class="col-3" style="text-align: right">
        Détail
    </div>
     <div class="col-2">
        Status
    </div>
</div>
EOF;

    }
    /**
     * @brief display the result of acc_operation_SQL with status
     */
    public function display_result()
    {
        static $int=0;
        $nb_error=0;
        if ($int == 0) {
            $this->display_col_header();

        }
        $int++;


        $cn=$this->acc_operation_sql->cn;
        $class=($int%2==0)?'even':'odd';
        echo '<div class="row '.$class.' "  style="padding:1rem;" >';
        echo '<div class="col">';
        echo substr($this->acc_operation_sql->getp("ao_date")??"", 0, 10);
        echo '</div>';
        echo '<div class="col-3">';
        echo $this->document_sql->getp("d_name");
        echo '</div>';
        echo '<div class="col">';
        echo $cn->get_value("select ad_value from fiche_detail where ad_id=23 and f_id=$1",
                [$this->acc_operation_sql->getp("supplier_id")]);
        echo '</div>';
        echo '<div class="col">';
        echo h($this->acc_operation_sql->getp("ao_label"));
        echo '</div>';

        echo '<div class="col" style="text-align: right">';
        echo nbm($cn->get_value("select sum(coalesce(aod_amount,0)) 
                    from noalyss_document.acc_operation_detail 
                    where acc_operation_id=$1", [$this->acc_operation_sql->acc_operation_id]),2);
        echo '</div>';
        $detail_info=$cn->get_array(" with qcode as (
                select ad_value,acc_operation_detail_id
                from fiche_detail
                join noalyss_document.acc_operation_detail 
                on (f_id = purchase_id) where ad_id=23)
        select  qcode.ad_value purchase_qcode,
               aod_amount,
               (select format('%s (%s/%s)',tva_label, tva_id::text,tva_code)  from tva_rate where tva_id=aod.tva_id ) tva_info
        from 
        noalyss_document.acc_operation_detail aod 
        join qcode using (acc_operation_detail_id)
        where acc_operation_id=$1", [$this->acc_operation_sql->acc_operation_id]);
        $nb_detail=count($detail_info);
        
        echo '<div class="col-3">';
        for ($e=0; $e<$nb_detail; $e++)
        {
            echo '<div class="row">';
            echo '<div class="col" style="text-align: right">';
            echo $detail_info[$e]['purchase_qcode'];
            echo '</div>';
            echo '<div class="col" style="text-align: right">';
            echo nbm($detail_info[$e]['aod_amount'],2);
            echo '</div>';
            echo '<div class="col">';
            echo $detail_info[$e]['tva_info'];
            echo '</div>';
            echo '</div>';
        }
        echo '</div>';

        echo '<div class="col-2">';
        echo Document_Operation::array_status()[$this->acc_operation_sql->getp("ao_status")];
        $ao_status=$this->acc_operation_sql->getp("ao_status");
        if ($ao_status == 1  ) {
            echo span(_("Déjà transféré"), 'class="notice"');
            
        }elseif($ao_status == 2) {
            echo span(h($this->acc_operation_sql->getp("ao_message")), 'class="warning"');
            $nb_error++;
        }
        else {
            echo span(h($this->acc_operation_sql->getp("ao_message")), 'class="notice"');
        }
        echo '</div>';
        echo '</div>';

        return $nb_error;
    }

    /**
     * @brief transform the acc_operation_Sql into an array we can use with Acc_Ledger_Purchase
     * @param int $p_ledger_id JRN_DEF.JRN_DEF_ID
     * @return array
     */
    public function transform($p_ledger_id)
    {
        global $g_parameter;
        $cn=$this->acc_operation_sql->cn;
        $supplier_id=$this->acc_operation_sql->getp('supplier_id');

        $array=array();
        $array['p_jrn']=$p_ledger_id;
        $array['e_ech']="";
        $array['e_pj']="";
        $array['e_pj_suggest']="";
        $array['e_mp']=0;
        $array['e_date']=substr($this->acc_operation_sql->getp('ao_date'), 0, 10);
        $array['e_client']=$cn->get_value("select ad_value from fiche_detail where ad_id=$1 and f_id=$2",
                [ATTR_DEF_QUICKCODE, $supplier_id]);
        $array['mt']=microtime(true);

// from db , retrieve data for acc_operation_detail
        $a_operation_detail=$cn->get_array("select  * from noalyss_document.acc_operation_detail 
                where 
                 acc_operation_id=$1", [$this->acc_operation_sql->getp("acc_operation_id")]);
        $array['nb_item']=count($a_operation_detail);

        for ($e=0; $e<$array['nb_item']; $e++)
        {
            $purchase_id=$a_operation_detail[$e]['purchase_id'];
            $amount=$a_operation_detail[$e]['aod_amount'];
            $tva_id=$a_operation_detail[$e]['tva_id'];

            $array['e_march'.$e]=$cn->get_value("select ad_value from fiche_detail where ad_id=$1 and f_id=$2",
                    [ATTR_DEF_QUICKCODE, $purchase_id]);


            $array['e_march'.$e.'_label']="";
            $row_tva=$cn->get_row("select tva_rate,tva_both_side from tva_rate where tva_id=$1", [$tva_id]);

            if ( $a_operation_detail[$e]['aod_amount_vat'] == 0 && $row_tva["tva_rate"] != 0 ) {
                $array['e_march'.$e.'_tva_amount']=bcdiv($amount, 1+$row_tva["tva_rate"], 2);
            } else {
                $array['e_march'.$e.'_tva_amount']=round($a_operation_detail[$e]['aod_amount_vat'],2);
            }
            $array['e_march'.$e.'_price']=bcsub($amount,  $array['e_march'.$e.'_tva_amount'],4);


            if ( $row_tva['tva_both_side'] == 1 && $g_parameter->MY_TVA_USE == 'Y') {
                $array['e_march'.$e.'_price']=$amount;
            }

            $array['e_quant'.$e]=1;
            $array['e_march'.$e.'_tva_id']=$tva_id;
        }

        $array['p_currency_rate']=1;
        $array['p_currency_code']=0;
        $array['e_comm']=$this->acc_operation_sql->getp('ao_label');
        $array['jrn_note_input']=$this->acc_operation_sql->getp("ao_note");
        // -------------- Add analytic -------------------------
        if ($g_parameter->MY_ANALYTIC!='nu') {
            $json=json_decode($this->acc_operation_sql->getp("ao_json"),true);
            $array=(! empty ($json) ) ? array_merge($array,$json):$array;
        }
        $array['other_info']=$this->acc_operation_sql->getp("ao_info");
        $array['bon_comm']="";
        $array['operation_tag']=$this->acc_operation_sql->getp("ao_tag");
        return $array;
    }

    /**
     * @brief transfer the selected document to accountancy, returns nb of document transferred to accountancy
     * 
     * @param array $pa_document_id DOCUMENT.DOCUMENT_ID
     * @param int JRN_DEF.JRN_DEF_ID
     * @param int 0 let the date 1 the first operation date will be the last of this ledger
     */
    static public function transfer_accountancy($pa_document_id, $p_ledger_id,$p_correct_date)
    {
        if (empty($pa_document_id))
        {
            return 0;
        }
// order depends of the date of the operation, 
        $cn=\Dossier::connect();
        $str_document_id=join(',', $pa_document_id);
        $a_document_id=$cn->get_array('select document_id,ao_date 
                    from noalyss_document."document" d 
                    join noalyss_document.acc_operation ao  using(document_id) 
                    where 
                    document_id in ('.\sql_string($str_document_id).') order by ao_date asc');
        if ($p_correct_date==1) {
            $max_date=$cn->get_value("select to_char(max(jr_date) ,'DDMMYY') from jrn where jr_def_id=$1",
            [$p_ledger_id]);
            $msg=_('date fact.');
            $cn->exec_sql("update noalyss_document.acc_operation 
                    set ao_date=to_date('$max_date','DDMMYY') ,
                        ao_label='$msg '||to_char(ao_date,'YY.MM.DD')||' '||coalesce(ao_label)
                    where 
                    document_id in (".\sql_string($str_document_id).") and ao_date < to_date($1,'DDMMYY')",
              [$max_date] );

        }
        foreach ($a_document_id as $ia_document_id)
        {
            $i_document_id=$ia_document_id['document_id'];
            $document_operation=Document_Operation::from_document_id($i_document_id);

            if ($document_operation->check_transferable($p_ledger_id))
            {
                $array=$document_operation->transform($p_ledger_id);
                $ledger=new \Acc_Ledger_Purchase(\Dossier::connect(), $p_ledger_id);
                $acc_operation_sql=$document_operation->getAcc_operation_sql();
                try
                {
                    $cn->start();
                    if ($acc_operation_sql->getp("ao_status")==3)
                    {
                        $receipt=$ledger->guess_pj();
                        $array['e_pj']=$array['e_pj_suggest']=$receipt;
                        $internal=$ledger->insert($array);
                        // cp  also the document LOBif the document is not empty
                        if ( !empty ($lob =  $document_operation->get_document_sql()->getp('d_lob'))) {
                            $cn->exec_sql("update jrn set jr_pj=$1,jr_pj_name=$2, jr_pj_type=$3 where jr_internal=$4  ",
                                    [
                                        $lob,
                                        $document_operation->get_document_sql()->getp('d_name'),
                                        $document_operation->get_document_sql()->getp('d_type'),
                                        $internal
                                    ]);

                        }
                        $acc_operation_sql->setp("ao_status", 1);
                        $acc_operation_sql->set("jr_internal", $internal);
                        $acc_operation_sql->save();
                        // add tag
                        $tag_str=$acc_operation_sql->getp("ao_tag");
                        if ( ! empty ($tag_str)) {
                            $tag_operation=new \Tag_Operation($cn);
                            $tag_operation->set_jrn_id($ledger->jr_id);
                            $aTag_id=explode(",",$tag_str);
                            foreach($aTag_id as $tag_id) {
                                $tag_operation->tag_add($tag_id);
                            }
                        }
                        $obj = new \Acc_Ledger_Info($cn);
                        $obj->save_extra($ledger->jr_id, $array);

                    }
                    $cn->commit();
                }
                catch (\Exception $exc)
                {
                    $cn->rollback();
                    echo $exc->getMessage();
                    $acc_operation_sql->setp("ao_status", 2);
                    $acc_operation_sql->setp("ao_message", mb_substr($exc->getMessage(),0,200));
                    $acc_operation_sql->save();
                }
            }
        }
    }

    /**
     * @brief returns all the operation_detail depending of the acc_operation_sql
     */
    function get_operation_detail()
    {
        global $cn;
        $a_detail_id=$cn->get_array("select acc_operation_detail_id from "
                ."noalyss_document.acc_operation_detail where acc_operation_id= $1",
                [$this->acc_operation_sql->getp("acc_operation_id")]);
        $a_return=[];
        foreach ($a_detail_id as $i_detail_id)
        {

            $a_return[]=Operation_Detail::build($i_detail_id['acc_operation_detail_id']);
        }
        return $a_return;
    }

    /**
     * @brief display details of all the related operation
     * @param int $p_fiche_def category of card
     * @param true | false $p_readonly
     * @param string $p_ident base to identify the operation
     */
    function display_detail($p_fiche_def, $p_readonly,$p_ident)
    {
        $a_detail=$this->get_operation_detail();
        $cnt=0;
        foreach ($a_detail as $i_detail)
        {
            $i_detail->display_row($this->document_sql->document_id, $p_fiche_def, $cnt, $p_readonly,$p_ident);
            $cnt++;
        }
        /* Add and empty row          */
        if (!$p_readonly)
        {
            $acc_operation_detail_sql=Operation_Detail::build(-1);
            $acc_operation_detail_sql->display_row($this->document_sql->document_id, $p_fiche_def, $cnt, $p_readonly,$p_ident);
        }
        return count($a_detail)+1;
    }
    /**
     * @brief delete operation_detail without a qcode
     */
    function clean_empty()
    {
        global $cn;
        $cn->exec_sql("delete from noalyss_document.acc_operation_detail where purchase_id is null");
    }

    /**
     * @brief check if the operation is already in the DB and returns an array with details about the possible
     * existing duplicate in NDER
     * @return array|null keys :
     *          - already_transfered (jr_id,jr_internal,qp_supplier,amount)
     *          - same_document ( acc_operation_id,ao_date,ao_label,ao_status)
     * @throws \Exception
     */
    function warning_duplicate()
    {
        global $cn;
        $sum=$cn->get_value("
        select sum(aod_amount) from noalyss_document.acc_operation_detail where
              acc_operation_id=$1",[$this->acc_operation_sql->acc_operation_id]);



        // find duplicate from already transferred
        $a_duplicate_transfered=$cn->get_array("
         select jr_id,jr_internal,qp_supplier,sum(qp_price+qp_vat+qp_nd_tva_recup+qp_nd_tva-qp_vat_sided) amount
from jrn jr1
join jrnx jx1 on (jr1.jr_grpt_id=jx1.j_grpt)
join quant_purchase qp on (qp.j_id=jx1.j_id)
where 
jr_date =to_date($1,'DD.MM.YYYY')
and qp_supplier=$2
and jr_internal <> $4
group by jr_id,jr_internal,qp_supplier
having sum(qp_price+qp_vat+qp_nd_tva_recup+qp_nd_tva-qp_vat_sided) = $3;
         ",[$this->acc_operation_sql->ao_date,$this->acc_operation_sql->supplier_id,$sum,$this->acc_operation_sql->jr_internal]);

        // find duplicate if the same document has been loaded more than once
        $a_duplicate_document=$cn->get_array("
select acc_operation_id,document_id,ao_date,ao_label,ao_status
from
    noalyss_document.document
left join noalyss_document.acc_operation using (document_id)
where d_size >0
  and d_name=$1
and  d_type=$2
and d_size=$3
and document_id != $4",[
            $this->document_sql->d_name
            ,$this->document_sql->d_type
            ,$this->document_sql->d_size
            ,$this->document_sql->document_id
        ]);

        if ( ! empty($a_duplicate_document) && $this->document_sql->d_size != 0 )
        {
            $result=array();
            $result['already_transfered']=$a_duplicate_transfered;
            $result['same_document']=$a_duplicate_document;
            return $result;
        }
        if ( $sum != null && $sum != 0 )
        {
            $result=array();
            $result['already_transfered']=$a_duplicate_transfered;
            $result['same_document']=$a_duplicate_document;
            return $result;
        }
        return null;


    }

    /**
     * @brief call warning_duplicate and display the result
     * @see Document_Operation::warning_duplicate()
     * @return void
     * @throws \Exception
     */
    function display_warning_duplicate()
    {
        $result=$this->warning_duplicate();

        if (empty ($result)) return;
        $style='style = "background-color:red;color:white;display:inline-block;margin:2px;padding:2px;font:100%;border-radius:6px"';

        if ( ! empty($result['already_transfered'])) {
            echo span("Attention",$style);
            echo span(_("Déja transféré ?"),'class="notice"');
            foreach ($result['already_transfered'] as $item_already_transfered) {
                echo \HtmlInput::detail_op($item_already_transfered["jr_id"], $item_already_transfered['jr_internal']);
            }
        }

        if ( ! empty($result['same_document'])) {
            if ( empty($result['already_transfered'] )) echo span("Attention",$style);
            $status=Document_Operation::array_status();
            $status[0]=_("Nouveau");
            echo span(_("Document similaire"),'class="notice"');
            foreach ($result['same_document'] as $item_same_document) {
                printf('<div style="margin-left:1rem;border:solid 1px gray; padding:0.1rem;display:inline-block;border-radius:5px">');
                printf( '<a href="javascript:void(0)" onclick="noalyss_document.box_recap(\'%s\')">',$item_same_document['document_id']);
                echo $item_same_document['ao_date']??"";
                echo " ".$item_same_document['ao_label']??"";
                if ( $item_same_document['ao_status'] != "" )
                    echo " ".   \Noalyss_Document\Document_Load::display_status($item_same_document['ao_status']);
                    else
                    echo " ".$status[0];
                echo '</A>';
                printf ("</div>");
            }
        }

    }
    /**
     * @brief Display a box with the detail of document
     * @see Document_Operation::warning_duplicate()
     * @return void
     */

    public function display_recap() {
        global $g_dir_noadoc;
        require_once $g_dir_noadoc."/template/document_operation-display_recap.php";

    }

    private function propose_copy_xmlpy()
    {
        global $g_dir_noadoc;
        require_once $g_dir_noadoc."/template/document_operation+propose_copy_xmlpy.php";
    }

    /**
     * @brief display a button to display detail of XMLPAYMENT
     * @param $xml_payment_detail int
     * @return int : 1 if no XMLPAYMENT, 0 : operation found , display it
     */
    function button_display_xmlpy($xml_payment_detail):int
    {
        if ($xml_payment_detail =='') return 1;

        global $g_dir_noadoc;

        $cn=$this->document_sql->cn;
        $detail = $cn->get_row("select * from xmlpayment.payment_detail where d_id=$1", [$xml_payment_detail]);
        require_once $g_dir_noadoc.'/template/document_operation-button_display_xmlpy.php';
        return 0;
    }

    function build_div_prefix() :string {
        return sprintf('prli%s', $this->document_sql->getp("document_id"));
    }
    function display_total()
    {
        /**
         * var : $op_id acc_operation_id int, -1 if not exist
         */
        $op_id= $this->acc_operation_sql->getp('acc_operation_id');
        $sum=0;
        if ( $op_id != -1) {
            $sum=$this->acc_operation_sql->get_cn()->get_value('
                select
                    sum(aod_amount) 
                from 
                    noalyss_document.acc_operation_detail aod 
                where 
                acc_operation_id = $1',[$op_id]);
        }
        printf("%s",nbm($sum));

    }
}
