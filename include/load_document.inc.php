<?php

namespace Noalyss_Document;

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2021) Author Dany De Bontridder <danydb@noalyss.eu>

if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');
global $cn, $gDossier, $g_plugin, $g_access, $g_dir_noadoc;

/**
 * @file
 * @brief load the document
 */
// present the files we have in document_noalyss.document

global $cn;
$document=new Document_Load($cn);
$url="ajax.php?".http_build_query(array("ac"=>$g_access
            , 'plugin_code'=>$g_plugin
            , "gDossier"=>$gDossier
            , 'act'=>'upload_file'));

$max_form=$document->get_max_post();
$msg=sprintf("Déposer les fichiers ici , taille max. par fichier = %s MB
        pour un total %s MB",
            round(MAX_FILE_SIZE/1024/1024,2),
            round($max_form/1024/1024,2)
        );
$msg=h($msg);
?>
<div class="container" style="margin-top:11px">
    <div class="row row-cols-1">
    <div  id="output" class="col" style="min-height: 200px;background-color: lavender" 
          ondragenter="document.getElementById('output').textContent = 'Drop for adding files';event.stopPropagation(); event.preventDefault();"
          ondragover="document.getElementById('output').textContent = 'Drop it!';event.stopPropagation();event.preventDefault();" 
          ondrop="event.stopPropagation();event.preventDefault();upload_by_div(event);">
        <?=$msg?>
    </div>
        
    <span id="result_upload_1b"><progress style="height:auto;width: 100%;appearance: none;" id="progress_upload1b" max="100" value="0"></span>
    <form  method="post" id = "upload_file_frm" enctype="multipart/form-data" onsubmit="return false;">
       <input type="hidden" name="MAX_FILE_SIZE" value="<?= MAX_FILE_SIZE ?>">
        <input type="file" name="FileUpload[]" id='FileUpload[]' multiple>
        <?php
        foreach (array("ac"=>$g_access, 'plugin_code'=>$g_plugin, "gDossier"=>$gDossier, 'act'=>'upload_file')
        as $key=> $value)
        {
            echo \HtmlInput::hidden($key, $value);
        }
        ?>
        <input type='SUBMIT' class="button" name="upload" value="Envoyer" onclick="return upload_form(this);">
    </form>
    <script>
        var post_max_size=parseInt(<?=$max_form?>);
         var max_size =<?= MAX_FILE_SIZE ?>;
        function upload_form(input) {
            waiting_box();
            var xhr = new XMLHttpRequest();
            var feedback_div = document.getElementById('output');
            document.getElementById("progress_upload1b").setAttribute("value", 0);

            xhr.upload.onprogress = function (e) {
                document.getElementById("progress_upload1b").setAttribute("max", e.total) + "<br/>";
                document.getElementById("progress_upload1b").setAttribute("value", e.loaded) + "<br/>";
              
            }
            xhr.upload.onload = function (e) {
               // document.getElementById("result_upload").innerHTML += "onload" + xhr.responseText + "<br/>";
            }
             // xhr.addEventListener('loadstart', handleEvent);
            xhr.onreadystatechange = function (event) {
//                document.getElementById("result_upload_1b").innerHTML += "onreadystatechange" + "<br/>";
                if (this.readyState == XMLHttpRequest.DONE)
                {
                    remove_waiting_box();
//                    document.getElementById("result_upload_1b").innerHTML += "ready=4" + "<br/>";
                    if (this.status === 200) {
                        document.getElementById("result_upload_1b").innerHTML += this.responseText + "<br/>";
                    } else {
                        document.getElementById("result_upload_1b").innerHTML += "status" + this.statusText + "<br/>";
                    }
                }
            }
            var file_to_upload=document.getElementById("FileUpload[]");
            var feedback_div = document.getElementById('output');
            
        
            var total_size=0;
            for (var e=0;e<file_to_upload.files.length;e++) {
              
                // check the size
                if (file_to_upload.files[e].size > max_size) {
                    // if size > accepted size , push filename with error in an array feedback,
                    feedback_div.innerHTML += '<p class="notice">' + file_to_upload.files[e].name + " est trop grand</p>";
                    remove_waiting_box();
                    return false;
                } else if ( total_size+file_to_upload.files[e].size >= post_max_size )
                {
                     feedback_div.innerHTML += '<p > limite '+post_max_size+" atteinte,supprimez des fichiers</p>";
                     remove_waiting_box();
                     return false;
                }
                else {
                    total_size+=file_to_upload.files[e].size;
                }
            }
            xhr.open("POST", "<?= $url ?>", true);
            // works xhr.send(new FormData(input.parentElement));
            var formData = new FormData(document.getElementById("upload_file_frm"));
            
            xhr.send(formData);
            
            return false;
        }    
        function upload_by_div(event) {
            waiting_box();
            var dt = event.dataTransfer;
            var files = dt.files;
            var count = files.length;
            var formData = new FormData();
            var parameter = {};
            var feedback_div = document.getElementById('output');
            document.getElementById("progress_upload1b").setAttribute("value", 0);
            var total_size=0;
            for (var i = 0; i < files.length; i++) {
                // check the size
                if (files[i].size > max_size) {
                    // if size > accepted size , push filename with error in an array feedback,
                    feedback_div.innerHTML += '<p class="notice">' + files[i].name + " est trop grand</p>";
                } else if ( total_size+files[i].size >= post_max_size )
                {
                     feedback_div.innerHTML += '<p > limite '+post_max_size+" atteinte</p>";
                     i=files.length;
                }
                else {
                    total_size+=files[i].size;
                    // if size correct, the append to parameter
                    formData.append('FileUpload[]', files[i]);
                    parameter[i] = {'name': files[i].name,
                        'size': files[i].size,
                        'type': typeof files[i]};
                    feedback_div.innerHTML += '<p >' + files[i].name + " accepted</p>";
                }
            }
            // if nothing to upload , end
            if (parameter.length == 0) {
                return;
            }
       
            var xhr = new XMLHttpRequest();
  
            xhr.upload.onprogress = function (e) {
                // document.getElementById("result_upload_1b").innerHTML += "loaded " + e.loaded + " total " + e.total + "<br/>";
                document.getElementById("progress_upload1b").setAttribute("max", e.total);
                document.getElementById("progress_upload1b").setAttribute("value", e.loaded);
         
            }
            xhr.upload.onload = function (e) {

                //    document.getElementById("result_upload_1b").innerHTML += "onload" + xhr.responseText + "<br/>";
            }
            xhr.onreadystatechange = function (event) {
                
//                document.getElementById("result_upload_1b").innerHTML += "onreadystatechange" + "<br/>";

                if (this.readyState == XMLHttpRequest.DONE)
                {
                    remove_waiting_box();
//                    document.getElementById("result_upload_1b").innerHTML += "ready=4" + "<br/>";
                    if (this.status === 200) {
                        document.getElementById("result_upload_1b").innerHTML += this.responseText + "<br/>";
                    } else {
                        document.getElementById("result_upload_1b").innerHTML += "status" + this.statusText + "<br/>";
                    }
                }
            }
            xhr.open("POST", "<?= $url ?>", true);
            xhr.send(formData);
        }
    </script>
</div>

