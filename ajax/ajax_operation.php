<?php

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * ²
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2021) Author Dany De Bontridder <danydb@noalyss.eu>

if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');

/**
 * @file
 * @brief  ajax about acc_operation
 */
/**
 * @var $act string inherited from ajax.php
 * @var $http HttpInput inherited from ajax.php
 * @var $cn Database inherited from ajax.php
 * @var $document_id int inherited from ajax.php
 */
if ($act=='acc_operation_save')
{
    try
    {
        global $g_parameter;
        $document_id=$http->get("document_id");
        $date_op=$http->get("date_op", "string", null);
        $code_supplier=$http->get("supplier".$document_id, "string", null);
        $card_supplier=new Fiche($cn);
        $card_supplier->get_by_qcode($code_supplier);
        $nb_item=$http->get("nb_item".$document_id);
        $note=$http->get('note');
        $ao_info=$http->get("ao_info");
        $ao_note=$http->get("ao_note");
        $ao_tag=$http->get("add".$document_id."tag","array",[]);


        $document_sql=new \Noalyss_Document\Document_SQL($cn, $document_id);
        $document_operation=new \Noalyss_Document\Document_Operation($document_sql);
        $acc_operation_sql=$document_operation->getAcc_operation_sql();

        // if already transfer to accountancy , refused to update
        if ($acc_operation_sql->getp("ao_status")==1)
        {
            throw new Exception(_("Déjà transféré"));
        }
        $qcode="";
        if ($card_supplier->id!=0 && !empty ($card_supplier->id) )
        {
            $acc_operation_sql->setp('supplier_id', $card_supplier->id);
            $qcode=$cn->get_value("select ad_value from fiche_detail where f_id=$1 and ad_id=23",
                    [$card_supplier->id]);
        }
        else
        {
            $acc_operation_sql->setp('supplier_id', '');
        }
        $acc_operation_sql->setp("document_id", $document_id);
        $acc_operation_sql->setp('ao_date', $date_op);
        $acc_operation_sql->setp('ao_label', $note);

        $acc_operation_sql->setp('ao_status', $http->request("ready","number",null));
        $acc_operation_sql->setp('ao_message', null);
        $acc_operation_sql->set("ao_info",$ao_info);
        $acc_operation_sql->set("ao_note",$ao_note);
        $list_tag_color="";
        $list_tag="";
        if ( ! empty ($ao_tag)) {
            $acc_operation_sql->setp("ao_tag",join(",",$ao_tag));
            $atag=[];$atagcolor=[];
            foreach ($ao_tag as $item_tag) {
                $row=$cn->get_row("select t_tag,t_color from tags where t_id=$1",[$item_tag]);
                $atag[]=$row['t_tag'];
                $atagcol[]=$row['t_color'];
            }
            $list_tag=join("~~",$atag);
            $list_tag_color=join("-",$atagcol);
        } else {
            $acc_operation_sql->setp("ao_tag","");
        }

        $acc_operation_sql->save();
        $acc_operation_id=$acc_operation_sql->getp("acc_operation_id");
        $cn->exec_sql("delete from noalyss_document.acc_operation_detail where acc_operation_id=$1",
                [$acc_operation_id]);
        $total_op = 0;
        for ($e=0; $e<$nb_item; $e++)
        {
            $acc_operation_detail_sql=new \Noalyss_Document\Acc_Operation_Detail_SQL($cn);
            $acc_operation_detail_sql->setp("aod_check",0);
            $code_purchase=$http->get("card_purchase".$document_id."_".$e, "string", null);
            if ( empty($code_purchase ) ) continue;
            $card_purchase=new Fiche($cn);
            $card_purchase->get_by_qcode($code_purchase);

            $tvac=$http->get("tvac".$document_id."_".$e, "string", null);
            $amount_vat=$http->get("amount_vat".$document_id."_".$e,"string","0");
            $amount_vat=(empty($amount_vat))?0:$amount_vat;

            $code_tva=$http->get('code_tva'.$document_id."_".$e, "string");

            $tva=Acc_Tva::build($cn, $code_tva);
            if ( $tva->tva_id ==-1)
            {
                $code_tva='';
            }

            if ($card_purchase->id!=0)
            {
                $acc_operation_detail_sql->setp('purchase_id', $card_purchase->id);
            }
            else
            {
                $acc_operation_detail_sql->setp('purchase_id', '');
            }

            $acc_operation_detail_sql->setp('aod_amount', $tvac);
            $acc_operation_detail_sql->setp("aod_amount_vat",$amount_vat);
            $total_op=bcadd($total_op,$tvac,2);
            $acc_operation_detail_sql->setp("acc_operation_id",$acc_operation_id);
           if  ($amount_vat != 0&&  $tva->tva_id ==-1 )
                throw new \Exception (sprintf(_("Le code TVA ne peut pas être vide"),$code_tva));

            if ( $amount_vat != 0 &&  $tva->tva_id ==-1)
                throw new \Exception (sprintf(_("TVA [ %s ] n'existe pas "),$code_tva));
            $acc_operation_detail_sql->setp('tva_id',$tva->tva_id);
            $acc_operation_detail_sql->save();
        }
        /**
         * Save also the ANC information
         */

        if ($g_parameter->MY_ANALYTIC!='nu') {
            $pa_id=$cn->get_value(
                "select string_agg(pa_id::text,',' order by pa_id) 
                from plan_analytique pa");

            $aPa_id=explode(",",$pa_id);

            $hplan=$http->request("hplan","array",[]);
            $val=$http->request("val","array",[]);
            $json=json_encode(['hplan'=>$hplan,'val'=>$val,'pa_id'=>$aPa_id]
                    ,JSON_NUMERIC_CHECK);
            $acc_operation_sql->setp("ao_json",$json);
            $acc_operation_sql->update();
        }
        $document_status=$http->request("ready","number",0);
        $follow_up=$cn->get_value( "select count(*) from noalyss_document.followup f where f.document_id=$1",
        [$document_id]);

        ob_start();

        \Noalyss_Document\Document_Load::display_recap(
            document_id:$document_id
            ,document_status: $document_status
            ,operation_date:$acc_operation_sql->ao_date
            ,filename:$document_sql->getp("d_name")
            ,qcode:$qcode
            ,label:$acc_operation_sql->getp('ao_label')
            , sum_operation:$total_op
            ,follow_up: $follow_up
            ,list_tag: $list_tag
            ,list_tag_color: $list_tag_color);

        $status=ob_get_contents();
        ob_end_clean();

        \header('Content-Type: application/json;charset=utf-8');
        echo \json_encode(['state'=>'OK', 'message'=>_("Enregistré")."  ".date('d.m.y H:i:s'),'status'=>$status],
                JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_NUMERIC_CHECK);

        return;
    }
    catch (Exception $ex)
    {
        \header('Content-Type: application/json;charset=utf-8');
        echo \json_encode(['state'=>'NOK', 'message'=>'<span class="warning">'. $ex->getMessage().'</span>'],
                JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_NUMERIC_CHECK);
        return;
    }
}
