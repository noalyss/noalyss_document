<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be

/*!\file
 * \brief raw file for PDF ewa
 */
 require_once 'noadoc_constant.php';


 /* export all cards in PDF */
 /* EXAMPLE
 if ( isset($_REQUEST['pdf_all']))
   {
     require_once('include/pdf_card.class.php');
     global $cn;
     $a=new Pdf_Card($cn);
     $a->setDossierInfo(dossier::id());
     $a->AliasNbPages('{nb}');
     $a->AddPage();
     $a->export();
     exit();
   }
  */

 /***
  * Display an image
  */
 if ( isset ($_REQUEST['display_image'])) {
        $http=new HttpInput();
        $document_id=$http->get("document_id","number");
        $document_sql=new \Noalyss_Document\Document_SQL($cn,$document_id);
        if ( $document_sql->document_id == -1) return;
        $filename=tempnam($_ENV['TMP'], "noadoc").$document_sql->d_name;
        $cn->start();
        $cn->lo_export($document_sql->d_lob,$filename);
        $cn->commit();
        $rotation=$http->get("rot","number",0);
        $a_rotation_value=[1=>-90,2=>90];
        if ($rotation != 0) {
            $info=getimagesize($filename);
            if ($info['mime']== "image/png") {
                $image = imagecreatefrompng($filename);

                if ($image != false) {
                    $imagerot = imagerotate($image, $a_rotation_value[$rotation],0);
                    imagepng($imagerot, $filename,-1);
                    $cn->start();
                    $oid=$cn->lo_import($filename);

                    $document_sql->setp("d_lob",$oid) ;
                    $document_sql->save();
                    $cn->commit();
                }


            } elseif ($info['mime'] == "image/jpeg") {
                $image = imagecreatefromjpeg($filename);
                if ($image != false) {
                    $imagerot = imagerotate($image, $a_rotation_value[$rotation],0);
                    imagejpeg($imagerot, $filename,100);
                    $cn->start();
                    $oid=$cn->lo_import($filename);

                    $document_sql->setp("d_lob",$oid) ;
                    $document_sql->save();
                    $cn->commit();
                }

            } elseif ($info['mime']== "image/gif") {
                $image = imagecreatefromgif($filename);
                if ($image != false) {
                    $imagerot = imagerotate($image, $a_rotation_value[$rotation],0);
                    imagegif($imagerot, $filename,100);
                    $cn->start();
                    $oid=$cn->lo_import($filename);

                    $document_sql->setp("d_lob",$oid) ;
                    $document_sql->save();
                    $cn->commit();
                }
            }
        }
        $handle_file=fopen($filename,'r');
        $content=fread($handle_file,filesize($filename));
        header ('Content-Type:'.$document_sql->d_type);
        echo $content;
     
 }
 
  /***
  * Preview the document if possible
  */
 if ( isset ($_REQUEST['preview_doc'])) {
        $http=new HttpInput();
        $document_id=$http->get("document_id","number");
        $document_sql=new \Noalyss_Document\Document_SQL($cn,$document_id);
        if ( $document_sql->document_id == -1) return;
        $filename=tempnam($_ENV['TMP'], "noadoc").$document_sql->getp("d_name");
      
        $cn->start();
        $cn->lo_export($document_sql->d_lob,$filename);
        $cn->commit();
        $filesize=filesize($filename);
        $handle_file=fopen($filename,'r');
        $content=fread($handle_file,$filesize);
        fclose($handle_file);
        ini_set('zlib.output_compression','On');
        header('Content-type: '.$document_sql->getp("d_type"));
        header('Content-Length: '.$filesize);
        header("Pragma: public");
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: must-revalidate");
        header("Accept-Ranges: bytes");
        echo $content;
     
 }

/***
 * Download the document
 */
if ( isset ($_REQUEST['download_doc'])) {
    $http=new HttpInput();
    $document_id=$http->get("document_id","number");
    $document_sql=new \Noalyss_Document\Document_SQL($cn,$document_id);
    if ( $document_sql->document_id == -1) return;
    $filename=tempnam($_ENV['TMP'], "noadoc").$document_sql->d_name;

    $cn->start();
    $cn->lo_export($document_sql->d_lob,$filename);
    $cn->commit();
    $filesize=filesize($filename);
    $handle_file=fopen($filename,'r');
    $content=fread($handle_file,$filesize);
    fclose($handle_file);
    // sanitize filename
    $name=str_replace(["'",'"'," ","/",":","\\","*"],"",$document_sql->getp("d_name"));
    ini_set('zlib.output_compression','On');
    header('Content-type: '.$document_sql->getp("d_type"));
    header('Content-Disposition: attachment; filename="'.$name.'"');
    header('Content-Length: '.$filesize);
    header("Pragma: public");
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: must-revalidate");
    header("Accept-Ranges: bytes");
    echo $content;

}