# Noalyss Document

NOALYSS_DOCUMENT est une extension de NOALYSS afin de charger des documents dans l'application et de permettre une saisie rapide des opérations d'achat.

L'extension est en 2 parties : 
a) Pour les mobiles (NDC) à mettre dans les menus "Mobiles / Smartphone", ne permet que le chargement de document
b) Pour les PC et tablettes (NDER), permet le chargement, la saisie et le transfert à la comptabilité.

## Installation

Il faut l'activer comme n'importe quel plugin , soit en  le téléchargeant depuis l'interface , soit en le décompressant
dans le répertoire noalyss/include/ext 

Ensuite dans Configuration -> Extension , vous l'activez pour les profils qui pourront l'utiliser

## Utilisation NDC 

Après l'avoir placé dans CFGPRO , dans menu smartphone , pour l'utiliser il vous suffit de vous  connecter avec votre login suivi de @mobile sur votre smartphone, vous pourrez prendre une photo et la charger automatiquement dans NOALYSS. 

Cependant, pensez à la taille de l'image , il vaut mieux utiliser une app qui scanne les documents plutôt que l'appareil photo.


## Utilisation NDER

Vous chargez (téléversez) vos fichiers, et vous pourrez ainsi les saisir pour votre comptabilité.  

Vous pouvez aussi enregistrer une opération dans document. Pour faire cela , Vous aller dans dans document , cliquez sur "Ajouter document" et vous cliquez sur "Chargement". On imagine que vous n'avez pas scanné le document.

*Attention* : Si l'utilisateur n'a aucun accès aux journaux d'achat, elle ne pourra pas transférer à la comptabilité, mais elle pourra préparer les opérations.

La sécurité sur les journaux de NOALYSS n'est pas contournable, ni pour les journaux ni pour les fiches.

## Paiement

Vous pouvez copier les informations de paiements directement dans XMLPY dans une liste en mode brouillon,
afin de composer un fichier avec la norme SEPA pour le déposer dans votre banque.

Il est conseillé d'ajouter l'attribut "Compte en Banque" à ceux que vous payez (fournisseurs, employés, ...)  afin
que l'information soit automatiquement ajouté



## Remarque 

Par défaut, vous ne voyez que les nouveaux documents, la liste vous permet de filtrer par statut : documents transférés, en erreur, ...

Plus d'information dans la FAQ

## Licence 
GNU GPL2
