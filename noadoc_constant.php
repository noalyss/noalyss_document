<?php
//This file is part of NOALYSS and is under GPL
//see licence.txt
/**
 *@file
 *Contains all the needed variable for the plugin
 *is name is plugin_name_constant.php
 * You can use some globale variable, especially for the database
 *  connection
 */
global $version_plugin;
$http=new HttpInput();

// compress image : width of PNG and JPG image 
define ("WIDTH",1200);
 
global $cn,$gDossier,$g_plugin,$g_access,$g_dir_noadoc;

$cn=\Dossier::connect();;
$http=new \HttpInput();
$g_dir_noadoc=__DIR__;

$gDossier=\Dossier::id();
$g_plugin=$http->request('plugin_code');
$g_access=$http->request('ac');
$version_plugin=\Extension::get_version(__DIR__."/plugin.xml",$http->request("ac"));
 function ndc_class_autoloader($class) {
     $class=strtolower($class);
 
     $a_class=[
         'noalyss_document\document_load'=>'document_load.php',
         'noalyss_document\plugin_install'=>'plugin_install.php',
         'noalyss_document\document'=>'document.php',
         'noalyss_document\document_operation'=>'document_operation.php',
         'noalyss_document\document_followup'=>'document_followup.php',
         'noalyss_document\operation_detail'=>'operation_detail.php',
         'noalyss_document\analytic'=>'analytic.php'
         ];
     
    if ( isset ($a_class[$class]) )  {
        $file=$a_class[$class];
        require_once __DIR__.'/class/'.$file;
        return;
    }
    $a_SQL = [
        'noalyss_document\document_sql'=>'document_sql.php',
        'noalyss_document\acc_operation_sql'=>'acc_operation_sql.php',
        'noalyss_document\acc_operation_detail_sql'=>'acc_operation_detail_sql.php',
        'noalyss_document\followup_sql'=>'followup_sql.php'
    ];
    if ( isset ($a_SQL[$class]) )  {
        $file=$a_SQL[$class];
        require_once __DIR__.'/database/'.$file;
        return;
    }
 }
 spl_autoload_register('\ndc_class_autoloader',true);
