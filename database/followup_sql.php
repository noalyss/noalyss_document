<?php

namespace Noalyss_Document;

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2021) Author Dany De Bontridder <danydb@noalyss.eu>

/**
 * @file
 * @brief ORM document
 */
class FollowUp_SQL extends \Table_Data_SQL
{

    function __construct(&$p_cn, $p_id=-1)
    {
        $this->table="noalyss_document.followup";
        $this->primary_key="fo_id";

        $this->name=array(
            "document_id"=>"document_id"
            , "fo_id"=>"fo_id"
            , "ag_id"=>'ag_id'
        );
        $this->type=array(
            "document_id"=>"number"
            , "fo_id"=>"number"
            , "ag_id"=>'number'
        );
        $this->default=array(
            "fo_id"=>"auto"
        );
        global $cn;

        parent::__construct($cn, $p_id);
    }

}
