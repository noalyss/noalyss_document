<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2024) Author Dany De Bontridder <danydb@noalyss.eu>

/**
 * @file
 * @brief Display the content of the row in display table, it is the recap of the operation
 * noalyss-form 8/04/24 11:35
 */
if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');

\Noalyss\Dbg::echo_file(__FILE__);

/**
 * @var $document_status int inherited from Document_Load->display_recard
 * @var $document_id  int inherited from Document_Load->display_recard
 * @var $operation_date  date (dd.mm.yy) inherited from Document_Load->display_recard
 * @var $filename  string  inherited from Document_Load->display_recard
 * @var $filename  string  inherited from Document_Load->display_recard
 * @var $qcode  string  inherited from Document_Load->display_recard
 * @var $label  string  inherited from Document_Load->display_recard
 * @var $list_tag_color  string  inherited from Document_Load->display_recard
 * @var $follow_up  int  inherited from Document_Load->display_recard
 * @var $sum_operation  float  inherited from Document_Load->display_recard
 */
\Noalyss_Document\Document_Load::display_status($document_status);

?>
<a href="javascript:void(0)" onclick="<?= sprintf('noalyss_document.more(this,\'%s\')', $document_id) ?>">

    <?php echo span(h($operation_date), 'style="margin-left:2rem"'); ?>
    [ <?= $filename ?>]
    <?= $qcode ?>
    <?php
    echo span(h($label));

    // display tags + color
    if ( ! empty($list_tag ) )
    {
        $alist_tag=explode("~~", $list_tag);
        $alist_tag_color=explode("-", $list_tag_color);
        $nb=count($alist_tag);
        for ($x=0;$x < $nb;$x++) {
            printf( '<span class="tagcell tagcell-color%s" >',$alist_tag_color[$x]);
            print h($alist_tag[$x]);
            print ('</span>');
        }
    }


    echo span(nbm($sum_operation, 2) . " € ", 'style="margin-left:4em;width:10rem;text-align:right;display:inline-block;float:right;"');

    ?>
    <?php if ($follow_up > 0 ) {
        printf(_("suivi [%s]"), $follow_up);
    } ?>
</a>