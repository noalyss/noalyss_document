<?php

namespace Noalyss_Document;

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2021) Author Dany De Bontridder <danydb@noalyss.eu>

if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');

/**
 * @file
 * @brief load the document
 */
// present the files we have in document_noalyss.document

global $cn,$g_user;
$document=new Document_Load($cn);
///--------------------------------------------------------------------------------------------------------------------
// Transfer
///--------------------------------------------------------------------------------------------------------------------
if ($http->post("sb", "string", "")=="transfer")
{
    $single_record=new \Single_Record("transfer_accountancy");
    if ($single_record->get_count()>0)
    {
        echo span(_("Doublon: opérations déjà transférées"), ' id="noadoc_dup_warning" class="warning" ');
    }
    else
    {
        $single_record->save();
        $a_document_id=$http->post("document_id");
        $ledger_id=$http->post("ledger_id", "number");
        $correct_date=$http->post("correct_date","number",0);
        $succes=Document_Operation::transfer_accountancy($a_document_id, $ledger_id,$correct_date);
    }
}
///--------------------------------------------------------------------------------------------------------------------
// Delete document
///--------------------------------------------------------------------------------------------------------------------
if (isset($_POST['delete_document_id']))
{
    $delete_document=$http->post("delete_document_id");
    \Noalyss_Document\Document_Load::delete_document($delete_document);
}
///--------------------------------------------------------------------------------------------------------------------
// Upload new Document
///--------------------------------------------------------------------------------------------------------------------
if (isset($_POST['upload_document_hid']))
{
    $single_record=new \Single_Record("uploading_document");
    if ($single_record->get_count()>0)
    {
        echo span(_("Doublon: documents déjà chargés"), ' id="noadoc_dup_warning" class="warning" ');
    }
    elseif (empty($_FILES['FileUpload']['name'][0]))
    {
        \Noalyss_Document\Document_Load::add_empty_document();
        $single_record->save();
    }
    else
    {
        $feedback=\Noalyss_Document\Document_Load::save_file("FileUpload");
        if ($feedback['nb_error']>0)
        {
            echo span(join(" - ", $feedback['msg_error']), 'class="warning"');
        }
        if ($feedback['nb_success']>0)
        {
            echo span(sprintf(_("Chargement réussi : %d fichier(s)"),$feedback['nb_success']),
                    'style="display:inline-block;margin-top:7px;background-color:green;'
                    . 'color:white;padding:5px ; border-radius:2px;"'),
            span(join(" - ", $feedback['msg_success']));
        }
        $single_record->save();
    }
}
///--------------------------------------------------------------------------------------------------------------------
/// Create a event in follow up for this
///--------------------------------------------------------------------------------------------------------------------
if ( isset($_POST['management_bt']) ) {
    $type_event=$http->post("type_event","number");
    $event_group=$http->post("event_group","number");
    $fup_title=$http->post("fup_title");
    // need to keep HTML
    $fup_summary=$_POST["fup_summary"];
    $fup_document_id=$http->post("fup_document_id");
    \Noalyss_Document\Document_FollowUp::insert($type_event,$event_group,$fup_title,$fup_summary,$fup_document_id);
}
// filter document, default : only the new
$filter_document_value=$http->request("filter_document_value", "number", 0);
$document->display_table($filter_document_value);
