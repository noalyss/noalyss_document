FAQ 
###

1. Si on efface des documents, ceux transférés à la comptabilité ou 
utilisés pour un événement sont-ils aussi effacés ?

Non , les documents transfèrés à la comptabilité ou utilisés dans un 
événement restent disponibles dans l'opération comptable et l'événement 
mais ne sont plus visibles dans l'extension

2. Faut-il effacer les opérations transférées ?

Il vaut mieux effacer quand vous n'en avez plus besoin , soit parce qu'elles sont 
dans la comptabilité , soit parce qu'elles sont dans la gestion.

3. Peut-on écrire dans un journal auquel on n'aurait pas eu accès ?

Non, la sécurité des journaux s'applique lors du transfert des opérations 
à la comptabilité.

4. Comment faire pour augmenter la taille des documents que l'on charge ?

Il faut changer MAX_FILE_SIZE dans noalyss/include/config.inc.php , 
https://wiki.noalyss.eu/doku.php?id=config.inc.php

5. Pourquoi on a un menu Chargement si on peut télécharger des 
documents dans "Document".

Chargement ne fait que du chargement, il est très utile lors qu'on a de 
nombreux documents à télécharger.

6. Comment reprendre tous les documents sur son PC ?

Il suffit de créér un événement avec tous les documents choisis. 
Ensuite cliquer sur l'un des documents et ouvrir l'événement 
puis cliquer sur "Télécharger tous les documents".

7. Comment introduire les numéros de pièces ?

Les numéros de pièces sont calculés automatiquement en fonction du journal.
Il est donc très important de ne pas transférer une opération qui est AVANT
la dernière opération du journal, sinon l'ordre chronologique et l'ordre des
pièces ne sera plus le même. Il y aura un avertissement avant de confirmer 
le transfert.

8. Avec l'appareil photo de mon smartphone, je n'arrive pas à charger les images

Probablement que les images sont trop grosses, il faut donc configurer php pour qu'ils les acceptent

9. Comment faire pour augmenter la limite de taille des documents ?

Dans php.ini, il faut changer la valeur de upload_max_filesize (voir 
https://www.php.net/manual/fr/features.file-upload.common-pitfalls.php )

et dans noalyss/include/config.inc.php  ajouter MAX_FILE_SIZE avec la même valeur en octet.

Exemple pour 4MB  (4*1024*1024)
define ("MAX_FILE_SIZE",4194304);


10. Quel est le format le plus adapté pour les documents ?

Il vaut mieux utiliser un outil pour scanner , ils permettent de crééer des PDF plus légers et en général, 
plus faciles à imprimer si nécessaire. Sur SmartPhone Android, il existe CamScanner, JetScan , TurboScan...

Pour les PDF  qui ne contiennent que du texte, un DPI de 115 est suffisant.

11. Des fiches fournisseurs ou services manquent , comment les ajouter ?

Ouvrez un onglet et aller sur CARD, les fiches que vous y ajouterez seront directement utilisables dans
l'extension




