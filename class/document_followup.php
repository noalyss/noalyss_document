<?php

namespace Noalyss_Document;

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2022) Author Dany De Bontridder <danydb@noalyss.eu>

/**
 * @file
 * @brief Manage FollowUp : create new event with selected documents
 */
class Document_FollowUp
{

    /**
     * @brief insert event with document
     * @param int $p_type_event type of event -> document_type
     * @param int $p_group -> profile 
     * @param string $p_title title of the action
     * @param string $p_summary summary of the action
     * @param string $p_docid list of document_id separated by comma
     */
    static public function insert($p_type_event, $p_group, $p_title, $p_summary, $p_docid)
    {
        try
        {
            $cn=\Dossier::connect();
            $cn->start();
            $followup=new \Follow_Up($cn);
            $followup->fromArray(array("ag_dest"=>$p_group, "ag_title"=>$p_title, "ag_comment"=>$p_summary,
                "dt_id"=>$p_type_event));
            $followup->save();
            $a_docid=explode(",", $p_docid);
            foreach ($a_docid as $iDocId)
            {
                $document_sql=new Document_SQL($cn, $iDocId);

                if ($document_sql->getp("d_size")==0)
                {
                    continue;
                }
                $dnumber=$cn->get_next_seq("seq_doc_type_".$p_type_event);
                $cn->exec_sql("insert into document(ag_id,d_lob, d_filename,d_mimetype,d_number) values ($1,$2,$3,$4,$5) ",
                        [$followup->ag_id
                            , $document_sql->getp("d_lob")
                            , $document_sql->getp("d_name")
                            , $document_sql->getp("d_type")
                            , $dnumber]);
                $followup_sql=new FollowUp_SQL($cn);
                $followup_sql->setp('document_id', $iDocId);
                $followup_sql->setp("ag_id", $followup->ag_id);
                $followup_sql->insert();
            }
            $cn->commit();
        }
        catch (\Exception $exc)
        {
            echo $exc->getMessage();
            $cn->rollback();
            error_log($exc->getTraceAsString());
        }
    }

}
