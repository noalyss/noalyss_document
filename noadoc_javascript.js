//This file is part of NOALYSS and is under GPL
//see licence.txt
/**
 *@brief javascript for plugin noalyss_document
 */
var noalyss_document = {
    /**
     * @function more display content
     * @param  DOMID p_iconid
     * @param int p_document_id
     * @returns {undefined}
     */
    more: function (p_iconid, p_document_id) {
        var col = document.getElementById("document_id" + p_document_id);

        if (col.parentNode.getAttribute("isVisible") == 1) {
            col.parentNode.setAttribute("isVisible", "0");
            col.parentNode.style = "display:none";
            $('row' + p_document_id).removeClassName("highlight2");
            return;
        }
        var next_document_id = 0;
        if ($('next_' + p_document_id)) {
            next_document_id = $('next_' + p_document_id).value;
        }
        new Ajax.Request("ajax.php", {
                method: 'get',
                parameters: {
                    act: 'more',
                    document_id: p_document_id,
                    gDossier: gDossier,
                    plugin_code: plugin_code,
                    ac: ac,
                    next_document_id: next_document_id
                },
                onSuccess: function (req) {
                    if (req.responseText == 'NOCONX') {
                        reconnect();
                        return;
                    }

                    var json = req.responseJSON;
                    var col = document.getElementById("document_id" + p_document_id);

                    if (!col) {
                        console.error("unknow document_id" + p_document_id)
                    }


                    col.parentNode.style = "display:table-row";
                    col.parentNode.setAttribute("isVisible", "1");

                    col.innerHTML = json['message'];
                    if (json.status == 'OK') {
                        Calendar.setup({
                            inputField: "date_op" + p_document_id,
                            ifFormat: "%d.%m.%Y",
                            button: "date_op" + p_document_id + "_trigger",
                            align: "Bl",
                            singleClick: true

                        });
                    }
                    json['message'].evalScripts();
                    $('row' + p_document_id).scrollTo();

                    $('row' + p_document_id).addClassName("highlight2");
                }
            }
        );
    },
    /**
     * Display a popup for selecting the VAT code
     * @param p_domid
     * @param p_document_id
     */
    popup_select_tva: function (p_domid, p_document_id, ident, loop) {
        p_domid['gDossier'] = gDossier;
        p_domid['ctl'] = 'code_tva' + p_document_id;
        p_domid['filter'] = 'purchase';
        p_domid.setAttribute("loop", loop);
        p_domid.setAttribute("ident", ident);

        popup_select_tva(p_domid, function () {
            var table_select_vat = $('tva_select_table');
            var aChildren = table_select_vat.childNodes[1].children;
            for (let i = 0; i < aChildren.length; i++) {
                aChildren[i].addEventListener("click", function () {
                    noalyss_document.compute_vat(p_domid);
                });
            }
        });
    },
    /**
     * Save the operation and update the DOM Elt feedback+document_id , col+document_id with the status
     *
     * @param p_form_id Dom Element of the FORM , contains document_id
     * @param call_function2 function calls if the ajax request succeeds
     * @param function2_param parameters for the function call_function2
     * @returns {boolean}
     */
    acc_operation_save(p_form_id, call_function2, function2_param) {
        try {
            new Ajax.Request("ajax.php", {
                method: "get",
                parameters: p_form_id.serialize() + 'act:acc_operation_save',
                onSuccess: function (req) {
                    if (req.responseText == 'NOCONX') {
                        reconnect();
                        return;
                    }

                    var json = req.responseJSON;
                    let document_id = p_form_id['document_id'].value;

                    $('feedback' + document_id).innerHTML = json['message'];
                    var col_id = "col" + p_form_id['document_id'].value;
                    $(col_id).innerHTML = json['status'];

                    try {

                    // calls the function given in parameters
                    if (call_function2) call_function2.apply(this, function2_param);
                    } catch (x) {
                        console.error (`ajax answer acc_operation_save ${x.message}`);
                    }
                }
            });
        } catch (e) {
            console.error("acc_operation_save failed");
            console.error(` %s`, e.message);
            if (p_form_id['document_id']) {
                let document_id = p_form_id['document_id'].value;
                document.getElementById("ind" + document_id).innerHTML = "<span class=\"warning\">" + e.message + "</span>";
            }
        }
        return false;
    },
    /**
     *  validate_operations before transferring to the accountancy
     * Try to validate acc_operation
     * @returns {undefined}
     */
    validate_operation: function () {
        try {
            //remove warning for duplicate if one exists
            if (document.getElementById("noadoc_dup_warning")) {
                removeDiv("noadoc_dup_warning");
            }

            var to_validate = noalyss_document.get_selected_file();
            if (to_validate.length == 0) {
                smoke.alert("Aucune sélection");
                return false;
            }
            $('document_validate_div').show();
            $('document_validate_div').innerHTML = "<p class=\"notice\">" + content[46] + "</p>" + loading();
            $('document_list_div').hide();
            waiting_box();
            new Ajax.Request("ajax.php", {
                    method: 'POST',
                    parameters: {
                        act: 'validate_operation',
                        "a_document[]": to_validate,
                        gDossier: gDossier,
                        plugin_code: plugin_code,
                        ac: ac,
                        p_jrn: $('validate_form')['p_jrn'].value
                    },
                    onSuccess: function (req) {
                        if (req.responseText == 'NOCONX') {
                            reconnect();
                            return;
                        }
                        $('document_validate_div').innerHTML = req.responseText;
                        req.responseText.evalScripts();
                        remove_waiting_box();
                    }
                }
            );
        } catch (e) {
            console.error(`error for ${e.message}`);
        }
        return false;
    },
    /**
     * @function apply the filter to the table of operations
     *
     */
    filter_table: function () {
        try {
            waiting_box();
            $("filter_document_value").value = $("filter_document").value;
            $("filter_table_frm").submit();
        } catch (e) {
            console.error(` filter_table ${e.message}`);
        }
        return false;
    },
    /**
     * @function returns array of selected documents
     * @returns array
     */
    get_selected_file: function () {
        var get_checked = document.getElementsByClassName("class_ck_document");
        var to_validate = [];
        for (var e = 0; e < get_checked.length; e++) {
            if (get_checked[e].checked) {
                to_validate.push(get_checked[e].value);
            }
        }
        return to_validate;
    },
    /**
     * @function delete selected documents
     * @returns {undefined}
     */
    delete_document: function () {
        smoke.confirm(content[50], function (e) {
            if (e) {
                try {

                    var to_validate = noalyss_document.get_selected_file();
                    if (to_validate.length == 0) {
                        smoke.alert("Aucune sélection");
                        return false;
                    }
                    $("delete_document_id").value = to_validate.join(",");
                    waiting_box();
                    $('delete_document_frm').submit();
                } catch (e) {
                    console.error(` delete_document ${e.message}`);
                }
                return false;
            }
        });
        return false;
    },
    /**
     * @function check size of the files before uploading
     * @param dom_id is the DOM ID of the form
     * @param str_file_id is the string of file input type
     */
    check_file_size: function dom_file_upload(dom_id, str_file_id) {
        try {
            let file = $(str_file_id).files;
            var max_size = $('document_max_size').value;
            for (var i = 0; i < file.length; i++) {
                let f = file[i];

                if (f.size > max_size) {
                    smoke.alert(f.name + content[78]);
                    return false;
                }
            }
            waiting_box();
            return true;
        } catch (e) {
            console.error(` check_file_size ${e.message}`);
        }
        return false;
    },
    form_document_show: function () {
        $('upload_document_div').show();
    },
    form_management_show: function () {
        $('management_document_div').show();
    },
    document_followup: function () {
        try {
            var to_validate = noalyss_document.get_selected_file();
            if (to_validate.length == 0) {
                smoke.alert("Aucune sélection");
                return false;
            }
            $("fup_document_id").value = to_validate.join(",");
            if ($("type_event").value == -1) {
                smoke.alert(content[49]);
                $("type_event").setStyle("border:2px solid red");
                return false;
            }
            waiting_box();
            return true;
        } catch (e) {
            console.error(` document_followup ${e.message}`);
            return false;
        }

    },
    /**
     * @function add a row for a document for purchase
     * @param int p_document_id
     * @param  p_fiche_def  category of card (fiche_def.fd_id) separated by comma
     * @returns {undefined}
     */
    row_add: function (p_document_id, p_fiche_def) {
        var nb = $("nb_item" + p_document_id).value;
        new Ajax.Request("ajax.php", {
                method: 'get',
                parameters: {
                    act: 'row_add',
                    document_id: p_document_id,
                    gDossier: gDossier,
                    plugin_code: plugin_code,
                    ac: ac,
                    nb_item: nb,
                    fiche_def: p_fiche_def
                },
                onSuccess: function (req) {

                    var txt = req.responseText;
                    var new_elt = document.createElement("p");
                    new_elt.innerHTML = txt;
                    $("detail" + p_document_id + "_div").appendChild(new_elt);
                    $("nb_item" + p_document_id).value = parseInt(nb) + 1;
                    txt.evalScripts();
                }
            }
        );
    },
    /**
     * @function rotation image
     */
    rotate: function (p_position, pn_document_id) {
        var image = $("image" + pn_document_id);
        var url_image = $("image" + pn_document_id).src
        var new_url = url_image.replace(/rot=[0-9]+/, "rot=" + p_position);
        image.src = "";
        image.src = new_url + '&x=' + Math.floor(Math.random() * 10000000000);
    },

    /**
     * Compute TVA, the DOMObject has the attribute loop and ident , used to computed the name of the cell
     * for the VAT Amount
     * @param domObject
     */
    compute_vat(domObject, object2) {

        var ident = domObject.getAttribute("ident");
        var loop = domObject.getAttribute("loop");

        new Ajax.Request("ajax.php", {
            method: "get",
            parameters: {
                act: 'compute_vat',
                gDossier: gDossier,
                plugin_code: plugin_code,
                ac: ac,
                tva_id: $('code_tva' + ident + "_" + loop).value,
                amount: $('tvac' + ident + "_" + loop).value
            },
            onSuccess: function (req) {
                if (req.responseText == 'NOCONX') {
                    reconnect();
                    return;
                }
                var json=req.responseJSON;
                $('amount_vat' + ident + "_" + loop).value = parseFloat(json.amount_vat);
                $('amount_reverse' + ident + "t" + loop).value = parseFloat(json.amount_reverse);

                const amount_novat = parseFloat($('tvac' + ident + "_" + loop).value) - parseFloat(json.amount_vat)+parseFloat(json.amount_reverse);

                $('amount_novat' + ident + "t" + loop).value = amount_novat;

                /**
                 * Cette partie fonctionne-t'elle ?
                 * @type {string}
                 */
                var css = "t" + loop + "-value-" + loop;
                var aElement = document.getElementsByClassName(css);
                var sum = 0;
                for (let i = 0; i < aElement.length; i++) {
                    let amount = parseFloat(aElement[i].value);
                    if (isNaN(amount)) amount = 0;
                    sum += amount;
                }

                $('nder_remain' + ident + 't' + loop).value = amount_novat - sum;

                anc_refresh_remain(ident + "t" + loop, loop);
            }
        });
    },
    /**
     * display detail of acc_operation in a small box
     */
    box_recap(pn_document_id) {

        try {
            var dgbox = "nder_box_recap";
            waiting_box();
            removeDiv(dgbox);
            // For form , most of the parameters are in the FORM
            // method is then POST
            //var queryString=$(p_form_id).serialize(true);

            var queryString = {
                act: 'box_recap',
                document_id: pn_document_id,
                gDossier: gDossier,
                plugin_code: plugin_code,
                ac: ac
            };
            var action = new Ajax.Request(
                "ajax.php",
                {
                    method: 'GET',
                    parameters: queryString,
                    onSuccess: function (req) {
                        remove_waiting_box();
                        if (req.responseText == 'NOCONX') {
                            reconnect();
                            return;
                        }

                        var div_style = "position:fixed;" + ";top:10%";
                        add_div({id: dgbox, cssclass: 'inner_box', html: loading(), style: div_style, drag: true});
                        var json = req.responseJSON;
                        $(dgbox).update(json['message']);

                    }
                }
            );
        } catch (e) {
            console.error(e.message);
            return false;
        }
    },
    copy_save_xml(pn_document_id,list_id) {
       try {

           let xml_list_id=list_id|| $('propose_listprli'+pn_document_id).value;
           noalyss_document.acc_operation_save($('acc_' + pn_document_id)
                , noalyss_document.copy_xmlpy
               , [pn_document_id,xml_list_id]);

       } catch(e) {
           console.error(e.message());
           return false;
       }
    },
    /**
     * copy information to XMLPY
     *
     */
    copy_xmlpy(pn_document_id,xml_list_id) {

        var queryString = {
            act: 'copy_xmlpy',
            document_id: pn_document_id,
            gDossier: gDossier,
            plugin_code: plugin_code,
            ac: ac,
            xml_list_id:xml_list_id
        };
        new Ajax.Request('ajax.php',{
            method:'get',
            parameters:queryString,
            onSuccess:function (req) {
                if (req.responseText != 'NOK' && req.responseText != '' )
                    $('prli'+pn_document_id+"_div").update(req.responseText)
                    $('prli'+pn_document_id+"_div").show();
            }
        })
    },
    /**
     * show XMPY info
     */
    display_xmlpy(pn_document_id) {
        var queryString = {
            act: 'display_xmlpy',
            document_id: pn_document_id,
            gDossier: gDossier,
            plugin_code: plugin_code,
            ac: ac
        };
        new Ajax.Request('ajax.php',{
            method:'get',
            parameters:queryString,
            onSuccess:function (req) {
                if (req.responseText != 'NOK' && req.responseText != '' )
                {
                    $('prli'+pn_document_id+"_div").update(req.responseText);
                    $('prli'+pn_document_id+"_div").show();
                }
            }
        })
    },
    /**
     * Update the sum("sum_operation"+ident) of the items  DOMObject has the attribute loop and ident , used to computed the name of the cell
     *
     * @param domObject is the document_id used to build the ID of DOMElement
     */
    update_sum(domObject) {
        var ident = domObject.getAttribute("ident");
        var aElement = document.getElementsByClassName('amt'+ident);
        var sum = 0;
        for (let i = 0; i < aElement.length; i++) {
            let amount = parseFloat(aElement[i].value);
            if (isNaN(amount)) amount = 0;
            sum += amount;
        }
        $('sum_operation'+ident).innerHTML=sum;
        new Effect.Highlight('sum_operation'+ident, {startcolor: "#c8b5f1"});
    }

};

function add_row(p_table, p_seq) {
    var mytable = g(p_table).tBodies[0];
    if (!mytable) {
        return;
    }
    var a_amount = document.getElementsByClassName(p_table + '-amount');
    var max = 0;
    for (var e = 0; e < a_amount.length; e++) {
        max += Math.abs(parseFloat(a_amount[e].value));
        e = a_amount.length + 1;
    }
    var amount = compute_total_table(p_table, p_seq);

    // For the detail view (modify_op) there is several form and then several time the
    // element
    var rowToCopy = mytable.rows[1];
    var row = mytable.insertRow(mytable.rows.length);

    for (var i = 0; i < rowToCopy.cells.length; i++) {
        var cell = row.insertCell(i);
        var txt = rowToCopy.cells[i].innerHTML;
        cell.innerHTML = txt;
    }

    var col = document.getElementsByClassName(p_table + '-value-' + p_seq);

    col[col.length - 1].value = 0;
    anc_refresh_remain(p_table, p_seq)
}

/**
 * Recompute total ANC and delta
 * @param string p_table format operation_id + "t" + loop
 * @param int loop
 */
function anc_refresh_remain(p_table, loop) {

    var css = p_table + "-value-" + loop;

    var aElement = document.getElementsByClassName(css);
    var sum = 0;
    for (let i = 0; i < aElement.length; i++) {
        let amount = parseFloat(aElement[i].value);
        if (isNaN(amount)) amount = 0;
        sum += amount;
    }
    $('nder_remain' + p_table).value = parseFloat($('amount_novat' + p_table).value) - sum;
    const novat = parseFloat($('amount_novat' + p_table).value);
    const delta = novat - sum;
    $('remain' + p_table).innerHTML = "novat : " + parseFloat(Math.round(novat * 100) / 100)
        + "  => reste " + parseFloat(Math.round(delta * 100) / 100);

    return;
}

/**
 * Set the remain amount to split between different anc account
 * @param p_obj DOM object , it the <INPUT TEXT> containing the amount of VAT or VAT included
 */
noalyss_document.set_remain_anc = function (p_obj) {
    const ident = p_obj.getAttribute("ident");
    const loop = p_obj.getAttribute("loop");
    $('nder_remain' + ident + 't' + loop).value = parseFloat(p_obj.value);
    const amount_novat = parseFloat($('tvac' + ident + "_" + loop).value) - parseFloat(p_obj.value)+parseFloat($('amount_reverse'+ident + "t" + loop).value);
    $('amount_novat' + ident + "t" + loop).value = amount_novat;
    anc_refresh_remain(ident + "t" + loop, loop);

};

function anc_key_choice(p_dossier, p_table, p_amount, p_ledger) {
    smoke.alert("Désactivé dans ce module");
}

function anc_key_clean(p_dossier, p_table, p_amount, p_ledger, p_jrnx_id, p_sequence) {
    var aValue = document.getElementsByClassName(p_table + "t" + p_sequence + "-value-" + p_sequence);

    for (let i = 0; i < aValue.length; i++) {
        aValue[i].value = 0;
    }
    $('remain' + p_table + "t" + p_sequence).innerHTML = p_amount;
    const table_name = p_table + 't' + p_sequence;
    const table_elt = $(table_name);
    if (table_elt.rows.length == 1) return;
    let nb_row = table_elt.rows.length;
    for (e = 2; e < nb_row; e++) {
        table_elt.deleteRow(2)
    }
}


