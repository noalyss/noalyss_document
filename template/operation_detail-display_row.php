<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2022) Author Dany De Bontridder <danydb@noalyss.eu>

if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');

/**
 * @file
 * @brief display one row per item for purchase 
 */
global $g_parameter;
\Noalyss\Dbg::echo_file(__FILE__);

// var $novat amount without VAT

$novat=bcsub($tvac->value??0,$amount_vat->value ??0,2);
/**
/// @var $reverse_vat float reverse vat
/// @var $novat float amount of VAT (HIDDEN "amount_novat".$p_document_id."t".$p_loop)
/// @var $amount_reverse
/// @var $this \Noalyss_Document\Operation_Detail
 */
?>
<p>
  
    <?= $card_purchase->input() ?>
    <?= $card_purchase->search() ?>
    <div style="display:inline-block">

    <?= $tvac->input() ?>
    <?= $code_tva->input() ?>
    <?=$amount_vat->input()?>
</div>

    <?=\HtmlInput::hidden("amount_novat".$p_document_id."t".$p_loop,$novat+$reverse_vat)?>
    <?=\HtmlInput::hidden("amount_reverse".$p_document_id."t".$p_loop,$reverse_vat)?>
    <?php    if ($g_parameter->MY_ANALYTIC!='nu') :?>
    <?php
        $div="div_{$p_ident}_{$p_loop}";
        ?>
        <button class='smallbutton'
            onclick="<?=sprintf("%s.show();anc_refresh_remain('%st%s',%s);return false;",$div,$p_ident,$p_loop,$p_loop)?>">
            C. Analytique <?=\Icon_Action::show_icon(uniqid(),""        );?>
        </button>

        <div class="" id="<?=$div?>" class="inner_box" style="display: none;margin-left:3rem;background-color: ghostwhite;justify-content: center;border:1px solid navy;padding: 1px">

        <?php
        /// var $remain_anc_value float : amount to use for ANC (without VAT + reverse VAT)
        $remain_anc_value=bcsub($tvac->value,$amount_vat->value,2);
        $remain_anc_value=bcadd($remain_anc_value,$reverse_vat,2);

        echo \HtmlInput::hidden($remain_anc_name,$remain_anc_value);
        ?>
        <h2 class="title"><?=_("Analytique")?></h2>
        <?php

        \Noalyss\Dbg::echo_var(1,"loop $p_loop");
        \Noalyss_Document\Analytic::display_row($p_document_id,$p_loop,$p_readonly,$p_ident,$novat+$reverse_vat);
        ?>
        <div>
            <?php echo \HtmlInput::button_hide($div);         ?>
        </div>
    </div>
    <?php endif;    ?>
</p>
