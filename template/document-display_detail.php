<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2021) Author Dany De Bontridder <danydb@noalyss.eu>

if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');
global $gDossier;
/**
 * @file
 * @brief display detail of operation and status
 * @see Noalyss_Document\Document::display_detail
 */
// prepare preview image : export and display
$preview="";$pdf="";
$flag_preview=true;
$http=new \HttpInput();
if ($this->document_sql->document_id>0
    &&$this->document_sql->d_lob!=""
    &&substr($this->document_sql->d_type, 0, 5)=="image"
)
{
    $document_id=$this->document_sql->document_id;
    $http_image="extension.raw.php?".http_build_query(array(
                "ac"=>$http->request('ac'),
                "gDossier"=>$http->request('gDossier'),
                "plugin_code"=>$http->request('plugin_code'),
                "document_id"=>$document_id,
                "display_image"=>"display_image",
                "rot"=>0,
                "rd"=>uniqid()
            )
    );
    $preview = sprintf('<a alt="%s"  title="%s"  class="icon smallbutton" href="#"  style="font-size:130%%"
  onclick="noalyss_document.rotate(2,%d);return false;">%s</a>',
        _("rotation à gauche"),_("rotation à gauche"),$document_id,"&#10226;");

    $preview .= sprintf('<a alt="%s" href="#"   title="%s"  class="icon smallbutton"  style="font-size:130%%" 
 onclick="noalyss_document.rotate(1,%s);return false;">%s</a>',
                    _(" rotation à droite"),_(" rotation droite"),$document_id,'&#10227;');

    $preview.="<img src=\"{$http_image}\"  width=\"100%\"  
id=\"image{$this->document_sql->document_id}\"> </img>";
}elseif ($this->document_sql->document_id>0
        && $this->document_sql->d_lob!=""
        && $this->document_sql->d_type == "application/pdf") {
    $url_get=http_build_query(array(
                "ac"=>$http->request('ac'),
                "gDossier"=>$http->request('gDossier'),
                "plugin_code"=>$http->request('plugin_code'),
                "document_id"=>$this->document_sql->document_id,
                "preview_doc"=>"preview_doc"
    ));
    $preview=<<<EOF

<embed
    src="extension.raw.php?{$url_get}"
    type="application/pdf"
    frameBorder="0px"
    scrolling="auto"
    height="100%"
    width="100%"
>

    </embed>
EOF;
    $url_get=http_build_query(array(
        "ac"=>$http->request('ac'),
        "gDossier"=>$http->request('gDossier'),
        "plugin_code"=>$http->request('plugin_code'),
        "document_id"=>$this->document_sql->document_id,
        "download_doc"=>"download_doc"

    ));
    $pdf=<<<EOF
            <a class="line" href="extension.raw.php?{$url_get}" target="_blank">{$this->document_sql->d_name}</a>
EOF;
}
elseif ($this->document_sql->d_size >0)
{
    $http_download="extension.raw.php?".http_build_query(array(
                "ac"=>$http->request('ac'),
                "gDossier"=>$http->request('gDossier'),
                "plugin_code"=>$http->request('plugin_code'),
                "document_id"=>$this->document_sql->document_id,
                "download_doc"=>"download_doc"
                    )
    );
    $preview=sprintf('<a class="line" href="%s" target="_blank">%s</a>', $http_download, h($this->document_sql->d_name));
     $flag_preview=false;
} else {
    $preview = _("Aucun document");
    $flag_preview=false;
}
$ident=$this->document_sql->document_id;
$document_operation=new \Noalyss_Document\Document_Operation($this->document_sql);
$class_preview="";

// if there is no preview then we dont reserve to much base
?>
    <?php
    if (  $flag_preview ) :?>
        <div class="row">
            <div class="col-lg-8 col-md-12 row_preview">
                <?=$preview ?>
            </div>
        <div class="col-md">

    <?php else : ?>
        <div class="row">
            <div class="col-12">
            <?=$preview ?>
            </div>
        </div>
        <div class="row">
        <div class="col-12">

    <?php endif; ?>
            <div>

                <?php
                $document_operation->display_warning_duplicate();


                ?>
            </div>

        <div class="">
        <?= $document_operation->input($next_document_id) ?>
        </div>
        <div id ="ind<?=$ident?>" class="row">

        </div>
        <div>
             <?=$pdf?>
        </div>
        <div>
            <?php
            echo _("Suivi");
            $cn=$this->document_sql->cn;
            $gestion=new \Default_Menu();
            $ac_gestion=$gestion->get("code_follow");
            $a_action=$cn->get_array("select distinct ag_id,ag_ref from action_gestion join noalyss_document.followup f using(ag_id)
                where
                document_id = $1",[$ident]);
            foreach($a_action as $i_action) {
                $alink="do.php?".\http_build_query(array("ac"=>$ac_gestion,"gDossier"=>$gDossier,"sa"=>"detail","ag_id"=>$i_action['ag_id']));
                printf ('<a class="line" target="_blank" href="%s"> %s </a>',$alink,$i_action["ag_ref"]);
            }

            ?>
        </div>

        </div>
    </div>
</div>
