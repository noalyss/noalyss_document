<?php

namespace Noalyss_Document;

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be

/**
 * @file
 * @brief install the  db schema 
 * 
 */
class Plugin_Install
{

    function __construct($cn)
    {
        $this->db=$cn;
    }

    function install()
    {
        global $g_dir_noadoc;
        $this->db->execute_script($g_dir_noadoc.'/sql/install.sql');
        $this->db->execute_script($g_dir_noadoc.'/sql/upgrade-1.sql');
    }

    function upgrade($p_version)
    {
        global $cn;
        global $g_dir_noadoc;
        $cur_version=$cn->get_value('select max(version_id) from noalyss_document.version');
        $cur_version++;
        
        for ($e=$cur_version; $e<=$p_version; $e++)
        {
            $version=$e-1;
            $this->db->execute_script($g_dir_noadoc.'/sql/upgrade-'.$version.'.sql');
        }
    }

}

?>
