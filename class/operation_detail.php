<?php
namespace Noalyss_Document;
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2022) Author Dany De Bontridder <danydb@noalyss.eu>
use \ICard;
use \INum;
use \ITva_Popup;
/**
 * @file
 * @brief Manage the operation detail , one per item
 */

class Operation_Detail
{
    private Acc_Operation_Detail_SQL $acc_operation_detail_sql;

    function __construct(Acc_Operation_Detail_SQL $p_acc_operation_detail_sql)
    {
        $this->acc_operation_detail_sql=$p_acc_operation_detail_sql;
    }

    static function build($p_acc_operation_detail_id) 
    {
        global $cn;
        $acc_operation_detail_sql=new Acc_Operation_Detail_SQL($cn,$p_acc_operation_detail_id);
        $operation_detail=new Operation_Detail($acc_operation_detail_sql);
        return $operation_detail;
    }
    /**
     * @brief display one row per item
     * @param int $p_document_id pk of noalyss_document.document
     * @param string $p_fiche_def category of card (fiche_def.fd_id) separated by comma
     * @param int $p_loop value in the loop
     * @param int $p_readonly true or false, to set in readonly mode
     * @param string $p_ident string to identify the operation, basis for DOMID of the row (DOCUMENT.DOCUMENT_ID)
     */
    function display_row($p_document_id,$p_fiche_def,$p_loop,$p_readonly,$p_ident)
    {
        global $cn,$g_dir_noadoc,$gDossier;
        global $g_parameter;
        $ident=$p_document_id;
        $card_purchase=new ICard("card_purchase".$ident."_".$p_loop);
        $card_purchase->set_typecard($p_fiche_def);
        $card_purchase->set_autocomplete(true);
        $card_purchase->style=sprintf(' placeholder="%s"', _("Service"));
        $card_purchase->set_choice("found_cards_div".$ident);
        $card_purchase->set_choice_create(false);
        $card_purchase->readOnly=$p_readonly;
        $card_purchase->set_limit(5);
        $purchase_id=$this->acc_operation_detail_sql->getp("purchase_id");
        $card_purchase->value=($purchase_id!="-1")?$cn->get_value("select ad_value from fiche_detail 
                 where ad_id=23 and f_id=$1", [$purchase_id]):"";
        $card_purchase->set_dblclick("fill_ipopcard(this);");
        $card_purchase->set_fct("null");
         
        $tvac=new INum("tvac".$ident."_".$p_loop);
        $tvac->size=11;
        $tvac->extra=sprintf(' placeholder="%s"', _("Montant TTC"));
        $tvac->value=$this->acc_operation_detail_sql->getp("aod_amount");
        $tvac->readOnly=$p_readonly;
        $tvac->set_attribute("loop",$p_loop);
        $tvac->set_attribute("ident",$ident);

        $remain_anc_name=$js_remain=$anc_amount="";
        if ($g_parameter->MY_ANALYTIC!='nu') {
            $remain_anc_name= "nder_remain".$p_document_id."t".$p_loop;
            $css_amount= $p_document_id."t".$p_loop."-amount";
            $js_remain="noalyss_document.set_remain_anc(this)";
        }
        $tvac->style=sprintf('class="inum input_text amt%s"',$ident);
        $tvac->set_attribute("remain_anc",$remain_anc_name);
        $tvac->javascript='onchange="format_number(this,2);noalyss_document.compute_vat(this);noalyss_document.update_sum(this);'.
            $js_remain.'"';

        $amount_vat=new INum("amount_vat".$ident."_".$p_loop);
        $amount_vat->extra=sprintf(' placeholder="%s"', _("Montant TVA"));
        $amount_vat->value=$this->acc_operation_detail_sql->getp("aod_amount_vat");
        $amount_vat->readOnly=$p_readonly;
        $amount_vat->size=11;
        $amount_vat->set_attribute("loop",$p_loop);
        $amount_vat->set_attribute("ident",$ident);
        $amount_vat->javascript='onchange="format_number(this,2);'.$js_remain.'"';

        $code_tva=new ITva_Popup("code_tva".$ident."_".$p_loop);
        $code_tva->set_attribute("gDossier", $gDossier);
        $code_tva->set_attribute("ctl", $code_tva->id);
        $code_tva->set_attribute("loop", $p_loop);
        $code_tva->set_attribute("ident", $ident);
        $code_tva->set_filter("purchase");
        $code_tva->but_javascript="noalyss_document.popup_select_tva(this,'{$ident}_{$p_loop}','{$ident}','{$p_loop}')";
        $code_tva->value=$this->acc_operation_detail_sql->getp("tva_id");
        $code_tva->readOnly=$p_readonly;
        $code_tva->js=' onchange="noalyss_document.compute_vat(this)"';

        $acc_tva=new \Acc_Tva($cn,$code_tva->value);
        /// var $reverse_vat : reverse vat
        $reverse_vat=($acc_tva->tva_both_side == 1)?$amount_vat->value:0;

         include $g_dir_noadoc."/template/operation_detail-display_row.php";
    }
 
}