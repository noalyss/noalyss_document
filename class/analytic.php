<?php
namespace Noalyss_Document;
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
// Copyright Author Dany De Bontridder danydb@aevalys.eu 19/11/22
/*! 
 * \file
 * \brief manage the analytic part, it display the detail per row, and convert it to save in accountancy
 */

class Analytic
{
    /**
     * @param int $p_document_id Document_SQL.document_id
     * @param int $sequence row number
     * @param bool $p_readonly true if it is readonly (already transfered) otherwise false
     * @param string $p_ident string to identify the operation
     * @return void
     */
    static function display_row($p_document_id,$sequence,$p_readonly,$p_ident,$amount_novat=0)
    {
        global $g_parameter;
        $cn=\Dossier::connect();
        if ($g_parameter->MY_ANALYTIC!='nu') {
            $document_Operation=new Document_Operation(new Document_SQL($cn,$p_document_id));

            $anc_operation=new \Anc_Operation($cn);
            $anc_operation->in_div=$p_ident;

            $json=json_decode($document_Operation->getAcc_operation_sql()->getp("ao_json")??"",1);

            if ( empty($json) && ! $p_readonly ) {
                echo $anc_operation->display_form_plan(null,1,1,
                    $sequence,$amount_novat,$p_ident);
            } elseif ( ! empty($json) && ! $p_readonly ){
                echo $anc_operation->display_form_plan($json
                       ,1,1,$sequence,$amount_novat,$p_ident);
            } elseif ( ! empty($json)  &&  $p_readonly  ) {
                echo $anc_operation->display_form_plan($json
                       ,1,0,$sequence,$amount_novat,$p_ident);
            }
        }
    }
}
