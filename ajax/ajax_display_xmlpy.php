<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
// Copyright Author Dany De Bontridder danydb@aevalys.eu 17/11/24
/*! 
 * \file
 * \brief 
 */

if (!defined('ALLOWED')) {
    die ("not allowed");
}

try {
    $document_id = $http->get("document_id", "number");
} catch (\Exception $e) {
    echo $e->getMessage();
    return;
}
$document_sql = new \Noalyss_Document\Document_SQL($cn, $document_id);
$document_operation = new \Noalyss_Document\Document_Operation($document_sql);
$div_prefix = $document_operation->build_div_prefix();
// get id of xmlpayment lists in Draft
$aList = $cn->get_array('select p_id,p_comment from xmlpayment.payment where p_status=$1 order by p_id desc', ['N']);


?>
<?php echo HtmlInput::title_box(_('Liste Paiements'), $div_prefix . '_div', 'hide'); ?>
    <p class="text-muted">
        <?php
        $url = '?' . http_build_query(['gDossier' => Dossier::id(), 'ac' => 'XMLPY', 'plugin_code' => 'XMLPY']);
        ?>
        copier les information dans une liste de paiement <a href="<?= $url ?>" class="line" target="_blank">XMLPY</a>
    </p>
<?php
// if there is no list in DRAFT then exit
$nb_list = count($aList);
if ($nb_list == 0) :
    echo _('Aucune liste de paiement en brouillon');
    ?>
    <ul class="aligned-block">

        <li>
            <?php
            echo \HtmlInput::button_hide($div_prefix . "_div");
            ?>

        </li>
    </ul>

    <?php
    return;
endif;
?>

<?php
$xml_payment_detail_id = $document_sql->getp("xml_payment_detail_id");
if ($document_operation->button_display_xmlpy($xml_payment_detail_id) != 0) :

    ?>
    <select id="propose_list<?= $div_prefix ?>">
        <?php
        for ($i = 0; $i < $nb_list; $i++):
            ?>

            <option value="<?= $aList[$i]['p_id'] ?>">
                <?= sprintf('[%s] %s ', $aList[$i]['p_id'], h($aList[$i]['p_comment'])) ?>
            </option>

        <?php
        endfor;
        ?>


    </select>
    <ul class="aligned-block">
        <li>
            <?php
            $js = sprintf("noalyss_document.copy_save_xml('%s');return false;"
                , $document_id);

            ?>
            <button class="smallbutton" onclick="<?= $js ?>">Copier vers XMLPY</button>
        </li>
        <li>
            <?php
            echo \HtmlInput::button_hide($div_prefix . "_div");
            ?>

        </li>
    </ul>

<?php
else:
    ?>
    <ul class="aligned-block">
        <li>
            <?php
            $xml_list_id = $cn->get_value('select payment_id from xmlpayment.payment_detail where d_id=$1 ',
                [$xml_payment_detail_id]);
            $js = sprintf("noalyss_document.copy_save_xml('%s','%s');return false;"
                , $document_id, $xml_list_id);

            ?>
            <button class="smallbutton" onclick="<?= $js ?>">Copier vers XMLPY</button>
        </li>
        <li>
            <?php
            echo \HtmlInput::button_hide($div_prefix . "_div");
            ?>

        </li>
    </ul>
<?php
endif;
?>