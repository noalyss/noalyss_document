<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
// Copyright Author Dany De Bontridder danydb@aevalys.eu 16/11/24
/*! 
 * \file
 * \brief propose list of XML SEPA
 */

/**
 * @var $this \Noalyss_Document\Document_Operation
 * @var $aList array of XMLPAYMENT.PAYMENT [ p_id, p_comment ]
 * @var $cn DatabaseCore from Document_Operation->propose_copy_xmlpy()
 */
if (!defined('ALLOWED')) {
    die ("not allowed");
}


// javascript object prefix
$div_prefix = $this->build_div_prefix();
?>
<button class="smallbutton" onclick="noalyss_document.display_xmlpy('<?=$this->document_sql->getp('document_id')?>')";return false;">Paiement XML</button>
<div id="<?= $div_prefix ?>_div" class="inner_box2" style="display:none">

</div>
