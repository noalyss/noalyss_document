<?php

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2022) Author Dany De Bontridder <danydb@noalyss.eu>

if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');

\Noalyss\Dbg::echo_file(__FILE__);
\Noalyss\Dbg::echo_var(1,$_FILES);
/**
 * @file
 * @brief Upload all the files
 */
$feedback=\Noalyss_Document\Document_Load::save_file("FileUpload");
if ($feedback['nb_error']>0)
{
    echo span(join(" - ", $feedback['msg_error']), 'class="warning"');
}
if ($feedback['nb_success']>0)
{
    echo span(_("Chargement réussi :")." ",
    'style="display:inline-block;margin-top:7px;background-color:green;color:white;padding:5px ; border-radius:2px;"'),
         span(join(" - ", $feedback['msg_success']));
}