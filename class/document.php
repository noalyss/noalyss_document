<?php
namespace Noalyss_Document;
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2022) Author Dany De Bontridder <danydb@noalyss.eu>


/**
 * @file
 * @brief handle document
 */
class Document 
{
    private $document_sql; //<! int document.document_id
    private $cn; //!< Database conx
    function __construct(\Database $p_cn,$p_document_id)
    {
        $this->document_sql=new Document_SQL($p_cn,$p_document_id);
        $this->cn=$p_cn;
    }
    public function getDocument_SQL()
    {
        return $this->document_sql;
    }

    public function setDocument_SQL(Document_SQl $p_document)
    {
        $this->document_sql=$p_document;
        return $this;
    }
    /**
     * @brief display the detail : of a document , preview + possibility to add or remove an operation for management
     * or accountancy
     */
    public function display_detail($next_document_id)
    {
        global $g_dir_noadoc;
        require_once $g_dir_noadoc."/template/document-display_detail.php";
    }


}