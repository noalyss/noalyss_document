<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
// Copyright Author Dany De Bontridder danydb@aevalys.eu 16/11/24
/*! 
 * \file
 * \brief copy to XMLPAYMENT
 */

/**
 * @var $document_id int PK of NOALYSS_DOCUMENT.DOCUMENT
 * @var $cn Database inherited from html/ajax.php
 * @var $http HttpInput inherited from html/ajax.php
 * @var $g_user Noalyss_User inherited from html/ajax.php
 */
if (!defined('ALLOWED')) {
    die ("not allowed");
}

try {
    $document_id = $http->get('document_id', 'number');
    $xml_list_id = $http->get('xml_list_id', 'number');
} catch (\Exception $e) {
    echo $e->getMessage();
    return;
}

// check if connected user can use XMLPY
if ($g_user->check_module("XMLPY") == 0) return;

$document_sql = new \Noalyss_Document\Document_SQL($cn, $document_id);

$row = $cn->get_row('
    select  sum(aod_amount) op_amount,ao_info,supplier_id
        from noalyss_document.acc_operation_detail aod 
        join noalyss_document.acc_operation ao on (aod.acc_operation_id =ao.acc_operation_id )
        join noalyss_document."document" d using (document_id) 
    where 
        document_id = $1
    group by ao_info,supplier_id
', [$document_id]);

// if there is nothing recorded, then return
if ( $row == null ) return ;

// get the card id (fiche.f_id) of the supplier (counterpart)
$supplier_id = ($row['supplier_id'] == "") ? 0 : $row['supplier_id'];


$fiche = new Fiche($cn, $supplier_id);
try {
    $cn->start();
    $payment_detail_id = $document_sql->getp('xml_payment_detail_id');

    // either insert a payment or update it
    if ($payment_detail_id == '') {
        $payment_detail_id = $cn->get_value('
        insert into xmlpayment.payment_detail (d_amount, d_bank_account, fiche_id, d_message, payment_id) 
        values ($1,$2,$3,$4,$5)
        returning  d_id',
            array($row['op_amount'],
                $fiche->strAttribut( ATTR_DEF_BQ_NO, 0),
                $supplier_id,
                $row['ao_info'],
                $xml_list_id
            )
        );
        $document_sql->setp('xml_payment_detail_id', $payment_detail_id);
        $document_sql->save();
    } else {
        $cn->exec_sql('update  xmlpayment.payment_detail set d_amount=$1, d_bank_account=$2, fiche_id=$3, d_message=$4 
        where d_id=$5
        ',
            array($row['op_amount'],
                $fiche->strAttribut( ATTR_DEF_BQ_NO, 0),
                $supplier_id,
                $row['ao_info'],
                $payment_detail_id));
    }
    $cn->commit();
} catch (\Exception $e) {
    $cn->rollback();
    echo $e->getMessage();
    return;
}


$document_operation=new \Noalyss_Document\Document_Operation($document_sql);
$div_prefix=$document_operation->build_div_prefix();
echo HtmlInput::title_box(_('Liste Paiements'), $div_prefix . '_div', 'hide');
?>
<p class="text-muted">
    <?php
    $url = '?' . http_build_query(['gDossier' => Dossier::id(), 'ac' => 'XMLPY', 'plugin_code' => 'XMLPY']);
    ?>
    copier les information dans une liste de paiement <a href="<?= $url ?>" class="line" target="_blank">XMLPY</a>
</p>
<?php
$document_operation->button_display_xmlpy($payment_detail_id);

// javascript object prefix

?>
<ul class="aligned-block">
    <li>
        <?php
        $js = sprintf("noalyss_document.copy_save_xml('%s','%s');return false;"
            ,$document_id,$xml_list_id);

        ?>
        <button class="smallbutton" onclick="<?=$js?>">Mise à jour vers XMLPY</button>
    </li>
    <li>
        <?php
        echo \HtmlInput::button_hide($div_prefix . "_div");
        ?>

    </li>
</ul>
