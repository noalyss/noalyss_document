begin;

alter table noalyss_document.acc_operation_detail add aod_amount_vat numeric(20,4);
comment on column noalyss_document.acc_operation_detail.aod_amount_vat is 'contains the amount of VAT ';
alter table noalyss_document.acc_operation_detail alter aod_amount_vat set default 0;
update noalyss_document.acc_operation_detail set aod_amount_vat=0;
alter table noalyss_document.acc_operation_detail alter aod_amount_vat set not null;

insert into noalyss_document.version values(4,'Possibility to add VAT manually',now());
commit;