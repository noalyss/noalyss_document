ALTER TABLE noalyss_document.acc_operation ADD ao_note text NULL;
COMMENT ON COLUMN noalyss_document.acc_operation.ao_note IS 'Extra info for operation';
ALTER TABLE noalyss_document.acc_operation ADD ao_tag text NULL;
COMMENT ON COLUMN noalyss_document.acc_operation.ao_tag IS 'List of tag id separated by comma';
ALTER TABLE noalyss_document.acc_operation ADD ao_info text NULL;
COMMENT ON COLUMN noalyss_document.acc_operation.ao_info IS 'Note for operation';
insert into noalyss_document.version values(6,'Supplemental info ',now());
commit;