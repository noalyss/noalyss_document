<?php

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be
/*
  /*!\file
 * \brief main file for : change
 *   - skel with code of the plugin
 *   - Route n : with real name
 * Global variable are defined into constant.php , by default
 *   - $g_dir : path of the plugin
 *   - $g_access : $_REQUEST['ac']
 *   - $g_plugin : $_REQUEST['plugin_code']
 *   - $gDossier : dossier id

 */

/*
 * load javascript
 */
/**
 * @var $http HttpInput inherited
 * @var $cn Database inherited
 */
if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');
require_once 'noadoc_constant.php';
global  $gDossier, $g_access, $g_plugin;
?>
<style>
    div.row_preview {
        min-height: auto;
    }
    /* SM */
    @media (min-width: 576px) {
        div.row_preview {
            min-height: auto;
        }
    }
    /* MD */
    @media (min-width: 768px) {
        div.row_preview {
            min-height: auto;
        }
    }
    /* LG */
    @media (min-width: 992px) {
        div.row_preview {
            min-height: 40rem;
        }
    }
    /* XL */
    @media (min-width: 1200px) {
        div.row_preview {
            min-height: 50rem;
        }
    }
    .nicEdit-main {
        background-color: white;
        border: 1px navy edge;
        border-color: darkgray;
        border-style: ridge;
        border-width: thin;
        overflow: auto !important;
    }
    .highlight2 {
        background-color: #81a8ca;
        color: white;
    }
    /**========*/
    .sum_operation {
        font-size:130%;
        border:1px solid navy;
        box-shadow: 3px 3px lightgrey;
        width:min(20rem,100%);
        padding:0.5rem;
        display: flex;
        flex-direction: row;
        justify-content: space-between;

    }
    /* LG */
    @media (min-width: 992px) {
        .sum_operation {
            margin-left:5%;
        }
    }

    }
</style>
<?php

ob_start();
require_once 'noadoc_javascript.js';
$j=ob_get_contents();
ob_end_clean();
$j.=sprintf(" var gDossier='%s'; var ac='%s';var plugin_code='%s';", $gDossier, $g_access, $g_plugin
);

echo create_script($j);

$url="?".http_build_query(["gDossier"=>$gDossier,
            "ac"=>$g_access,
            "plugin_code"=>$g_plugin]);

$menu=array(
    array($url.'&sa=mang_doc', _('Document'), _('Comptabilité, gestion et chargement des documents'), "mang_doc"),
    array($url.'&sa=upload', _('Chargement'), _('Chargement documents'), "upload")
);

$sa=$http->request("sa", "string", "xx");

$cn=Dossier::connect();
if ($cn->exist_schema('noalyss_document')==false)
{
    $iplugn=new Noalyss_Document\Plugin_Install($cn);
    $iplugn->install();
}
$version=$cn->get_value("select max(version_id) from noalyss_document.version");
$current_version=7;
if ($version<$current_version)
{
    $iplugn=new Noalyss_Document\Plugin_Install($cn);
    $iplugn->upgrade($current_version);
}
// if exists schema xmlpayment, check that the FK is available, if needed create it
if ( $cn->exist_schema('xmlpayment') &&
    $cn->get_value("select count(*) from   pg_constraint where conname='fk_xmlpayment'") == 0) {
        $cn->exec_sql("
    alter table noalyss_document.document
    add constraint fk_xmlpayment
        foreign key (xml_payment_detail_id) 
            references xmlpayment.payment_detail(d_id) on delete set null on update cascade");


}
// show menu
echo '<div style="float:right"><a class="mtitle" style="font-size:140%" href="http://wiki.noalyss.eu/doku.php?id=noalyss_document" target="_blank">Aide</a>'.
'<span style="font-size:0.8em;color:red;display:inline">vers:'.$version_plugin.'</span>'.
'</div>';
echo '<div >';
echo ShowItem($menu, 'H', 'nav-item ', 'nav-link', $sa, 'nav nav-pills nav-level2 noprint');
echo '</div>';

if ($sa=='mang_doc')
{
    require_once('include/manage_document.inc.php');
    exit();
}

if ($sa=="upload")
{
    require_once('include/load_document.inc.php');
    exit();
}
?>
