<?php

namespace Noalyss_Document;

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2021) Author Dany De Bontridder <danydb@noalyss.eu>

/**
 * @file
 * @brief ORM document
 */
class Acc_Operation_SQL extends \Table_Data_SQL
{

    function __construct(&$p_cn, $p_id=-1)
    {
        $this->table="noalyss_document.acc_operation";
        $this->primary_key="acc_operation_id";

        $this->name=array(
            "acc_operation_id"=>"acc_operation_id"
            , "ao_date"=>"ao_date"
            , 'ao_label'=>'ao_label'
            , "supplier_id"=>'supplier_id'
            , 'document_id'=>'document_id'
            , 'ao_saved'=>'ao_saved'
            , 'ao_status'=>'ao_status'
            , 'ao_message'=>'ao_message'
            ,'jr_internal'=>'jr_internal'
            ,'ao_json'=>"ao_json"
            ,'ao_note'=>'ao_note'
            ,'ao_info'=>'ao_info'
            ,'ao_tag'=>'ao_tag'
        );
        $this->type=array(
           "acc_operation_id"=>"acc_operation_id"
            , "ao_date"=>"date"
            , 'ao_label'=>'text'
            , "supplier_id"=>'numeric'
            , 'document_id'=>'numeric'
            , 'ao_saved'=>'date'
            , 'ao_status'=>'numeric'
            , 'ao_message'=>'text'
            ,'jr_internal'=>'text'
            ,'ao_json'=>"text"
            ,'ao_note'=>'text'
            ,'ao_info'=>'text'
            ,'ao_tag'=>'text'

        );
        $this->default=array(
            "acc_operation_id"=>"auto",
            "oa_saved"=>"auto"
        );
        $this->date_format="DD.MM.YYYY HH24:MI:SS";
        global $cn;

        parent::__construct($cn, $p_id);
    }

}
