<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
// Copyright Author Dany De Bontridder danydb@aevalys.eu 6/11/22
/*! 
 * \file
 * \brief compute the vat
 */
if ( ! defined ('ALLOWED') ) {     die ("not allowed"); }

try {
    $http = new \HttpInput();
    $amount = $http->get("amount", "string");
    $tva_id = $http->get("tva_id", "string");
} catch (Exception $e) {
    echo $e->getMessage();
    \Noalyss\Dbg::echo_var(1, $e->getTraceAsString());
    return;
}
if ( empty($amount) || empty($tva_id))  { echo "0"; return;}
if ( empty($tva_id)) echo 0;
global $cn;
$tva=Acc_Tva::build($cn, $tva_id);
if ($tva->tva_id == -1 )  { echo "0"; return;}
if (  $tva->tva_both_side == 0)
{
    // normal VAT
    $resultat =bcdiv($amount,1+$tva->tva_rate,4);
    $tva_amount =round(bcsub($amount,$resultat,4),2);
    $amount_reverse=0;
}
else
{
    // there is A reverse VAT
    $tva_amount =bcmul($amount,$tva->tva_rate,4);
    $amount_reverse=$tva_amount;
}

json_response(array('amount_vat'=>$tva_amount,'amount_reverse'=>$amount_reverse));

