<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
// Copyright Author Dany De Bontridder danydb@aevalys.eu 25/07/24
/*! 
 * \file
 * \brief display a recap of a document from NDER, only from noalyss_document.document or also from
 * noalyss_document.acc_operation
 *
 */
global $cn;
$http = new HttpInput();

$div = "nder_box_recap";

?>


<?php echo HtmlInput::title_box(_("détail"), $div) ?>
<div class="p-1">
    <?php
    // detail of the operation:
    if ($this->acc_operation_sql->acc_operation_id != -1) :
        $acc_operation_detail = $cn->get_array("
            select 
ao_label , to_char(ao_date,'DD.MM.YY') strdate,ao_status,supplier_id,purchase_id,aod_amount, tva_id,aod_amount_vat,ao_tag ,jr_internal
from noalyss_document.acc_operation ao join noalyss_document.acc_operation_detail aod 
using (acc_operation_id)
            where ao.acc_operation_id=$1
        ", [$this->acc_operation_sql->acc_operation_id]);

        ?>
        <?php
        $first = $acc_operation_detail[0];
        $supplier = new \Fiche ($cn, $first['supplier_id']);
        $label = $first['ao_label'];
        $date = $first['strdate'];
        $amount = 0;
        foreach ($acc_operation_detail as $row) {

            $amount = bcadd($amount, $row['aod_amount'] ?? 0);

        }
        $strtag = "";
        if (!empty($first['ao_tag'])) {

            $aTag_id = explode(",", $first['ao_tag']);
            foreach ($aTag_id as $tag_id) {
                $tag = new \Tag($cn, $tag_id);

                $strtag .= $tag->display();
            }
        }

        ?>
        <h3>Information Fournisseur</h3>
        <div class="row m-1">
            <div class="col">
                <span class="text-black-50">Date</span>
                <?= $date ?>

            </div>
            <div class="col">
                <span class="text-black-50">Fournisseur</span>
                <?= $supplier->getName() ?>
                <?= $supplier->strAttribut(ATTR_DEF_FIRST_NAME) ?>
                <?= $supplier->strAttribut(ATTR_DEF_QUICKCODE) ?>
            </div>
            <div class="col">
                <span class="text-black-50">Libellé</span>
                <?= $label ?>
            </div>
            <div class="col">
                <span class="text-black-50">Montant</span>
                Total = <?= $amount ?>
            </div>

        </div>
        <div class="row">
            <div class="col">

                <?=\Noalyss_Document\Document_Load::display_status($first['ao_status']) ?>
                <?php
                if ( $first['ao_status'] == 1) :
                    $jr_id=$cn->get_value("select jr_id from jrn where jr_internal=$1",[$first['jr_internal']]);
                    echo \HtmlInput::detail_op($jr_id, $first['jr_internal']);
                endif;
                ?>
            </div>
            <div class="col m-2">

                <span class="text-black-50">Etiquette</span>
                <?= $strtag ?>
            </div>
        </div>

        <h3><?= _("Articles") ?></h3>

        <?php foreach ($acc_operation_detail as $row) : ?>

        <?php
        $fiche = new Fiche($cn, $row['purchase_id']);
        $tva = new \Acc_Tva($cn, $row['tva_id']);
        ?>
        <div class="row m-1">
            <div class="col">
                <span class="text-black-50">Article </span>
                <?= $fiche->getName() ?>


                <?= $fiche->get_quick_code() ?>
            </div>
            <div class="col">
                <span class="text-black-50">Montant </span>
                <?= nb($row['aod_amount']); ?>

            </div>
            <div class="col">
                <span class="text-black-50">TVA </span>

                <?= $tva->tva_code ?>
                ( <?= $tva->tva_id ?> )
                <?= $tva->tva_label ?>

            </div>


        </div>
    <?php endforeach; ?>

    <?php

    endif;
    ?>

    <h3>Fichier</h3>
    <div class="row m-1">
        <div class="col">

            <?php
            // link to download the document
            $url_get = http_build_query(array(
                "ac" => $http->request('ac'),
                "gDossier" => $http->request('gDossier'),
                "plugin_code" => $http->request('plugin_code'),
                "document_id" => $this->document_sql->document_id,
                "download_doc" => "download_doc"

            ));
            $pdf = <<<EOF
            <a class="line" href="extension.raw.php?{$url_get}" target="_blank">{$this->document_sql->d_name}</a>
EOF;
            ?>
            <?= $pdf ?>
        </div>
    </div>
    <ul class="aligned-block">
        <ul>
            <?= HtmlInput::button_close($div) ?>

        </ul>

    </ul>
</div>
