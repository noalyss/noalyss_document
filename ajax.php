<?php

//This file is part of NOALYSS and is under GPL
//see licence.txt
require_once "noadoc_constant.php";

$http=new \HttpInput();
try
{
    $act=$http->request("act");
    $a_action=['more'=>'ajax_more.php',
                "acc_operation_save"=>"ajax_operation.php",
                "validate_operation"=>"ajax_validate_operation.php",
                "row_add"=>"ajax_row_add.php",
                "upload_file"=>"ajax_upload_file.php",
                "box_recap"=>"ajax_box_recap.php",
        "copy_xmlpy"=>'ajax_copy_xmlpy.php',
        "display_xmlpy"=>'ajax_display_xmlpy.php',
        "compute_vat"=>"ajax_compute_vat.php"];

    if (array_key_exists($act, $a_action))
    {

        $file=$a_action[$act];
        require_once 'ajax/'.$file;
        return;
    }
    else
    {
        throw new Exception(_("Action invalide"));
    }
}
catch (Exception $e)
{
    record_log($e->getMessage());
    record_log($e->getTraceAsString());
}
