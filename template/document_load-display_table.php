<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2021) Author Dany De Bontridder <danydb@noalyss.eu>
/**
 * @file
 * @see Noalyss\Document->display_table
 */
if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');

\Noalyss\Dbg::echo_file(__FILE__);
global $g_user, $cn;
?>

<div id="document_validate_div">

</div>
<div id="document_list_div">
    <div class="row">
        <div class="col">

            <form method="get" id="validate_form" onsubmit="noalyss_document.validate_operation();return false;">
                <?= \HtmlInput::request_to_hidden(['gDossier', 'ac', 'plugin_code']) ?>
                <?php
                // available purchase ledger

                $a_ledger=$g_user->get_ledger('ACH', 2, false);
                if (empty($a_ledger) || count($a_ledger)==0)
                {
                    echo '<div class="smallbutton notice" style="color:gray">';
                    echo _("Aucun journal disponible");
                    echo '</div>';
                }
                else
                {
                    $select=new \ISelect("p_jrn");
                    $a_value=[];
                    foreach ($a_ledger as $i_ledger)
                    {
                        $a_value[]=array('label'=>$i_ledger['jrn_def_name'], 'value'=>$i_ledger['jrn_def_id']);
                    }
                    $select->value=$a_value;
                    ?>
                    <?= $select->input() ?>
                    <?= HtmlInput::submit("save", _("Transférer à la comptabilité")) ?>
                <?php } // end of else ?> 
            </form>
        </div>
        <div class="col">
            <form method="POST" id="delete_document_frm" onsubmit="return noalyss_document.delete_document();">
                <?= \HtmlInput::request_to_hidden(['gDossier', 'ac', 'plugin_code']) ?>
                <?php
                echo \HtmlInput::hidden("delete_document_id", "");
                ?>

                <?= HtmlInput::submit("delete_bt", _("Effacer ")) ?>
                <?= \Icon_Action::tips("Effacement uniquement dans l'extension, ne changera rien à la comptabilité") ?>


            </form>
        </div>
        <div class="col">
            <?=
            \HtmlInput::button_Action(_("Ajouter documents"), 'return noalyss_document.form_document_show()')
            ?>
            <div class="inner_box" style='margin-right:20%;min-width: 60%;position:fixed;display:none' 
                 id='upload_document_div'>
                     <?= \HtmlInput::title_box(_("Chargement"), 'upload_document_div', "hide") ?>
                <form method="POST" id="upload_document_frm" 
                      enctype="multipart/form-data" 
                      onsubmit="return noalyss_document.check_file_size(this, 'FileUpload[]')">
                          <?=
                          \HtmlInput::request_to_hidden(['gDossier', 'ac', 'plugin_code',
                              'sa'])
                          ?>
                    <p>
                        <?=
                        _("Vous pouvez choisir plusieurs documents, si aucun n'est choisi, vous créerez".
                                " un document vide, ce qui vous permettra de saisir une opération sans document ")
                        ?>
                    </p>
                    <?php
                    echo \HtmlInput::hidden("upload_document_hid", "");
                    echo \HtmlInput::hidden("document_max_size", MAX_FILE_SIZE);
                    $single_record=new \Single_Record("uploading_document");
                    echo $single_record->hidden();
                    ?>
                    <input type="file" name="FileUpload[]" id='FileUpload[]' multiple>
                    <ul class='aligned-block'>
                        <li>
                            <?= HtmlInput::submit("upload_bt", _("Chargement")) ?>
                        </li>
                        <li>
                            <?= HtmlInput::button_hide('upload_document_div') ?>
                        </li>
                    </ul>
                </form>
            </div>


        </div>
        <?php
            /**
             * find writable profile for document
             */
            if ($g_user->getAdmin()!=1)
            {
                $sql=" (select p_granted "
                        ."     from user_sec_action_profile "
                        ."     where ua_right in ('W','O') and p_id=".$g_user->get_profile().") ";
            }
            else
            {
                $sql="(select p_id from profile)";
            }
            $a_writable_profile=$cn->make_array("select  p_id as value, ".
                                  "p_name as label ".
                                  " from profile  "
                                  ."where "
                                  ."p_id in "
                                  .$sql
                                  ."order by 2");
            
        ?>
        <div class='col'>
            <?php
            if (  empty($a_writable_profile)) :
            ?>
                <div class="smallbutton notice" style="color:gray">
                    <?=_("Aucun profil de gestion")?>
                </div>
            <?php else : ?>
            <?=
                \HtmlInput::button_Action(_("Ajout dans gestion"), 'return noalyss_document.form_management_show()')
            ?>
            <div class="inner_box" style='margin-right:20%;min-width: 60%;position:fixed;display:none' 
                 id='management_document_div'>
                     <?= \HtmlInput::title_box(_("Gestion"), 'management_document_div', "hide") ?>
                <form method='POST'
                      id='management_document_frm'
                      onsubmit="return noalyss_document.document_followup();"
                      >
                          <?php
                          $title=new \IText("fup_title");
                          $title->size=60;
                          $title->placeholder=_('title');
                          $summary=new \ITextArea("fup_summary");
                          $summary->id=uniqid();
                          $summary->set_enrichText("enrich");
                          $summary->style='class="input_text field_follow_up" style="height:21rem;width:80rem;margin-left:1em;background-color:white;"';

                          $type=new ISelect("type_event");
                          $type->name="type_event";
                          $type->value=$cn->make_array("select dt_id,dt_value from document_type order by dt_value", 1);
                          $type->selected=0;
                          $single_record=new \Single_Record("followup_doc");
                          echo $single_record->hidden();
                          $profile=new ISelect('event_group');
                          $profile->value=$a_writable_profile;
                          ?>
                    <div class="row">
                        <div class="col-2">
                              <?= _("Titre")?>
                        </div>
                        <div class="col">
                             <?=$title->input() ?>
                          </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                        <?=_("Type document")?> 
                        </div>
                        <div class="col">
                         <?= $type->input() ?>
                        </div>
                     </div>
                   <div class="row">
                        <div class="col-2">
                        <?=_("Profile")?>
                        </div>
                        <div class="col">
                          <?= $profile->input() ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 m-4">
                          <?= $summary->input() ?>
                          <?php
                          echo \HtmlInput::hidden("fup_document_id", "");
                          ?>
                        </div>
                    </div>
                    <ul class='aligned-block'>
                        <li>
                            <?= HtmlInput::submit("management_bt", _("Chargement")) ?>
                        </li>
                        <li>
                            <?= HtmlInput::button_hide('management_document_div') ?>
                        </li>
                    </ul>
                </form>
            </div>
            <script>
                (function(){$('<?=$summary->id?>').setStyle="background-color:white;"})();
            </script>
            <?php endif;?>
        </div>
    </div>
    <?php
    echo \Dossier::hidden();

    $sel_filter=new \ISelect("filter_document");
    $sel_filter->value=array(
        ["label"=>_("Nouveau"), "value"=>0],
        ["label"=>_("Tous"), "value"=>-1],
        ["label"=>_("Erreur"), "value"=>2],
        ["label"=>_("Prêt"), "value"=>3],
        ["label"=>_("Transférés"), "value"=>1]
    );
    $sel_filter->selected=$p_filter;
    $sel_filter->javascript='onchange="noalyss_document.filter_table()"';

    echo _("Filtre"), $sel_filter->input();
    echo \HtmlInput::button("filter_bt", _("Filtrer"), sprintf('onclick="return noalyss_document.filter_table();"'));

    $checkbox=new \ICheckBox("ck_document[]");
    $checkbox->set_range("class_ck_document");

    echo \HtmlInput::filter_table('tb_uploaded_doc', '1,2', 1);
    ?>
    <form method="get" id="filter_table_frm">
        <?= \HtmlInput::request_to_hidden(["gDossier", "ac", "plugin_code", "sa"]) ?>
        <input type="hidden" name="filter_document_value" id="filter_document_value">
    </form>

    <table class="result " id="tb_uploaded_doc">
        <thead>
        <th class="">
            <?= _("Détail") ?>
        </th>
        <th class="">
            <?=$sort_table->get_header(0) ?>
            <span style="margin-left:4em;width:10rem;text-align:right;display:inline-block;float:right;">
                <?=$sort_table->get_header(2)?>
            </span>
        </th>
        <th>
            <?=$sort_table->get_header(1) ?>
        </th>
        <th class="">
        </th>
        </thead>
        <tbody>
            <?php
            $a_row=\Database::fetch_all($ret);
            for ($i=0; $i<$max_row; $i++):
                $row=$a_row[$i];
                $uploaded_date=\Datetime::createFromFormat('Y-m-d H:i:s', substr($row['d_uploaded_time'], 0, 19));
                ?>
                <tr id="row<?=$row['document_id']?>">
                    <td>
                        <?=
                        \Icon_Action::more(uniqid(), sprintf('noalyss_document.more(this,\'%s\')', $row['document_id']))
                        ?>
                    </td>
                    <td id="col<?=$row['document_id']?>">

                        <?php

                        
                        ?>
                        <?php
                        \Noalyss_Document\Document_Load::display_recap(
                                document_id:$row['document_id'],
                                document_status: $row['ao_status'],
                                operation_date: $row['ao_date'],
                                filename: $row['d_name'],
                                qcode: $row['qcode'],
                                label: $row['ao_label'],
                                sum_operation: $row['sum_op'],
                                follow_up: $row['cnt_follow'],
                                list_tag: $row['list_tag'],
                                list_tag_color: $row['list_tag_color']);
                        ?>
                    </td>
                        <?php if (get_class($uploaded_date)=='DateTime'): ?>
                        <td sorttable_customkey="<?= $uploaded_date->format('ymd-His') ?>">
                        <?= $uploaded_date->format('d.m.y H:i:s') ?>
                        </td>
                        <?php endif; ?>
                    <td>
                    <?php
                    $checkbox->value=$row['document_id'];
                    echo $checkbox->input();
                    if ( $i < $max_row -1 ) {
                        echo \HtmlInput::hidden(sprintf("next_%s",$a_row[$i]['document_id']),$a_row[$i+1]['document_id']);
                    }
                    ?>
                    </td>
                </tr>
                <tr style="display:none">
                    <td colspan="3" id="document_id<?= $row['document_id'] ?>">

                    </td>

                </tr>
<?php endfor; ?>
        </tbody>
    </table>
</div>
<?=
$checkbox->javascript_set_range('class_ck_document')?>
