<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2021) Author Dany De Bontridder <danydb@noalyss.eu>

if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');
/**
 * @file
 * @brief load the document
 */
require_once 'noadoc_constant.php';
ob_start();
require_once 'noadoc_javascript.js';
$j=ob_get_contents();
ob_end_clean();
echo create_script($j);

// if a file is sent , then save
if (isset($_FILES['ndc_file']))
{
    $result=Noalyss_Document\Document_Load::save_file("ndc_file");
    if ($result['nb_error']>0)
    {
        echo '<span class="warning">';
        echo _("Echec"), " ", join(" ,", $result['msg_error']);
        echo '</span>';
    }
    if ($result['nb_success']>0)
    {
        echo '<span class="warning" style="background-color:darkgreen;animation-name:null">';
        echo _("Succés"), " ".join(" , ", $result['msg_success']);
        echo '</span>';
    }
}
$document=new \Noalyss_Document\Document_Load($cn);
$max_form=$document->get_max_post();
$max_size=round(MAX_FILE_SIZE/1024/1024, 2);
?>
<div id="output">
    
</div>
<div class="">
    <FORM METHOD="POST" action="do.php" enctype="multipart/form-data" id="document_form" onsubmit="return check_size();">
        <?php
        echo \HtmlInput::request_to_hidden(['sa', 'plugin_code', 'gDossier', 'ac']);
        ?>
        <input type="file" name="ndc_file[]" id='ndc_file[]' multiple>
        <?= \HtmlInput::submit('loadoc', _("Sauver")) ?>
    </FORM>

</div>  
<script>
    var max_size = parseInt(<?= MAX_FILE_SIZE ?>);
    var post_max_size = parseInt(<?= $max_form ?>);
    var feedback_div = document.getElementById('output');
    function check_size()
    {
        waiting_box();
        var fFile = document.getElementById("ndc_file[]");
        var total_size=0;
        for (var e = 0; e < fFile.files.length; e++) {
            
            // check the size
            if (fFile.files[e].size > max_size) {
                // if size > accepted size , push filename with error in an array feedback,
                feedback_div.innerHTML += '<p class="notice">' + fFile.files[e].name + " est trop grand</p>";
                remove_waiting_box();
                return false;
            } else if (total_size + fFile.files[e].size >= post_max_size)
            {
                feedback_div.innerHTML += '<p > limite ' + post_max_size + " atteinte,supprimez des fichiers</p>";
                remove_waiting_box();
                return false;
            } else {
                total_size += fFile.files[e].size;
            }
        }
        return true;
    }
    document.getElementById("ndc_file[]").addEventListener("change", function ()
    {
        if (!check_size())
            return false;
        document.getElementById("document_form").submit();
        return true;
    });
</script>
